﻿using Apprendre.Controllers;
using Apprendre.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
namespace Apprendre.Tests.Controllers
{
  [TestClass]
  public class CardControllerTests
  {
    [TestMethod]
    public void EditCardReturnsViewResultWithCardModel()
    {
      var mock = new Mock<ICardService>();
      mock.Setup(x => x.Get(3)).Returns(new Card());

      var controller = new CardsController(mock.Object);

      // Act
      ViewResult vr = controller.Edit(3) as ViewResult;
      ActionResult vr7 = controller.Edit(7);

      Assert.IsNotNull(vr.Model);
      Assert.IsInstanceOfType(vr7, typeof(HttpNotFoundResult));
      mock.Verify(x => x.Get(3), Times.Once());
    }






    private ControllerContext GetControllerContextMock()
    {
      var identityMock = new Mock<IIdentity>();
      identityMock.Setup(x => x.Name).Returns("Username1");

      var userMock = new Mock<IPrincipal>();
      userMock.Setup(p => p.Identity).Returns(identityMock.Object);

      var contextMock = new Mock<HttpContextBase>();
      contextMock.SetupGet(ctx => ctx.User).Returns(userMock.Object);

      var ccMock = new Mock<ControllerContext>();
      ccMock.SetupGet(con => con.HttpContext).Returns(contextMock.Object);

      return ccMock.Object;
    }

    [TestMethod]
    public void EditCardReturnsErrorString()
    {
      var mock = new Mock<ICardService>();
      mock.Setup(x => x.Update(new Card(), "1")).Returns(false);

      var controller = new CardsController(mock.Object);
      controller.ControllerContext = GetControllerContextMock();

      // Act
      string result = controller.Edit(new Card());

      Assert.AreEqual(result, HtmlUtils.Error);
    }

    [TestMethod]
    public void EditCardReturnsSuccessfulString()
    {
      // Assign/arrange
      var mock = new Mock<ICardService>();
      var card = new Card()
      {
        Id = 1,
        Word = "Word",
        Translation = "Translation",
        Dictionary = new Dictionary()
      };
      mock.Setup(x => x.Update(card, "Username1")).Returns(true);

      var controller = new CardsController(mock.Object);
      controller.ControllerContext = GetControllerContextMock();
            
      // Act
      string result = controller.Edit(card);

      // Assert
      Assert.AreEqual(result, HtmlUtils.Successful);      
    }


    [TestMethod]
    public void AddCardsReturnsViewResultWithSelectListModel()
    {
      var mock = new Mock<ICardService>();
      mock.Setup(x => x.GetAvailableDictionaries()).Returns(new List<Dictionary>{
        new Dictionary{ Id = 1, Name = "Dic 1"},
        new Dictionary{ Id = 2, Name = "Dic 2"},
        new Dictionary{ Id = 3, Name = "Dic 3"}
      });

      CardsController controller = new CardsController(mock.Object);

      // Act
      ViewResult result = controller.Add(2, 10) as ViewResult;

      Assert.IsNotNull(result);
      Assert.IsNotNull(result.Model);
      List<SelectListItem> list = result.Model as List<SelectListItem>;
      Assert.AreEqual(list.Count(), 3);
      Assert.AreEqual(list.FirstOrDefault(x => x.Selected).Value, "2");
    }


    [TestMethod]
    public void AddCardsReturnsViewModelWithErrorVM()
    {
      var mock = new Mock<ICardService>();
      mock.Setup(x => x.AddCards(1, "word\ttranslation")).Returns(1);
      mock.Setup(x => x.AddCards(1, "word")).Returns(0);  

      CardsController controller = new CardsController(mock.Object);

      // Act
      var result2 = controller.Add(9, "word\ttranslation") as ViewResult;
      var result3 = controller.Add(1, "word") as ViewResult;
      var result4 = controller.Add(9, "word") as ViewResult;
      var result5 = controller.Add(1, "") as ViewResult;
      var result6 = controller.Add(9, "") as ViewResult;

      Assert.IsNotNull(result2);
      Assert.IsNotNull(result3);
      Assert.IsNotNull(result4);
      Assert.IsNotNull(result5);
      Assert.IsNotNull(result6);

      Assert.IsInstanceOfType(result2.Model, typeof(ErrorVM));
      Assert.IsInstanceOfType(result3.Model, typeof(ErrorVM));
      Assert.IsInstanceOfType(result4.Model, typeof(ErrorVM));
      Assert.IsInstanceOfType(result5.Model, typeof(ErrorVM));
      Assert.IsInstanceOfType(result6.Model, typeof(ErrorVM));
    }

    [TestMethod]
    public void AddCardsReturnsRedirectToAction()
    {
      var mock = new Mock<ICardService>();
      mock.Setup(x => x.AddCards(1, "word\ttranslation")).Returns(1);
      mock.Setup(x => x.AddCards(1, "word")).Returns(0);

      CardsController controller = new CardsController(mock.Object);

      // Act
      var result1 = controller.Add(1, "word\ttranslation") as RedirectToRouteResult;

      Assert.IsNotNull(result1);
    }





  }
}