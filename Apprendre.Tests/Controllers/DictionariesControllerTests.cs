﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Apprendre.Controllers;
using Apprendre.Models;
using System.Web.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace Apprendre.Tests
{
  [TestClass]
  public class DictionariesControllerTests
  {
    [TestMethod]
    public void AddDicReturnsViewResultWithNullModel()
    {
      // Arrange
      var mock = new Mock<IDictionaryService>();
      DictionariesController controller = new DictionariesController(mock.Object);

      // Act
      ViewResult result1 = controller.Add(new Nullable<int>()) as ViewResult;
      ViewResult result2 = controller.Add(5) as ViewResult;

      // Assert
      Assert.IsNotNull(result1);
      Assert.IsNull(result1.Model);

      Assert.IsNotNull(result2);
      Assert.IsNull(result2.Model);
      Assert.AreEqual(result2.ViewData["Added"], 5);
    }

    [TestMethod]
    public void ViewListOfDicReturnsViewResultWithListOfDictionaryVM()
    {
      var mock = new Mock<IDictionaryService>();
      mock.Setup(x => x.GetAllDictionariesVM()).Returns(new List<DictionaryBase>() 
      { 
        new DictionaryBase() { Id = 1, Name = "Dic 1", Total = 100 },
        new DictionaryBase() { Id = 2, Name = "Dic 2", Total = 200 },        
        new DictionaryBase() { Id = 3, Name = "Dic 3", Total = 300 } 
      });
      DictionariesController controller = new DictionariesController(mock.Object);

      // Act
      ViewResult result = controller.View(null, null) as ViewResult;

      Assert.AreEqual(result.ViewName, "List");
      Assert.IsNotNull(result.Model);
      Assert.IsInstanceOfType(result.Model, typeof(IEnumerable<DictionaryBase>));
      Assert.AreEqual((result.Model as IEnumerable<DictionaryBase>).Count(), 3);
    }

    [TestMethod]
    public void GetDicByIdReturnsViewResultWithDictionaryVM()
    {
      var mock = new Mock<IDictionaryService>();
      mock.Setup(x => x.GetDictionary(new DictionaryRV() { Id = 5 }, 0)).Returns(
        new DictionaryVM
        {
          Dictionary = new Dictionary
          {
            Id = 5,
            Name = "Dic 5",
            Cards = new List<Card> { new Card(), new Card(), new Card() }
          }
        });

      DictionariesController controller = new DictionariesController(mock.Object);

      // Act
      ViewResult result = controller.View(new DictionaryRV() { Id = 5 }, 0) as ViewResult;

      Assert.IsNotNull(result);
      Assert.AreEqual(result.ViewName, "");
      Assert.IsNotNull(result.Model);
      Assert.IsInstanceOfType(result.Model, typeof(DictionaryBase));
      Assert.AreEqual((result.Model as DictionaryVM).Dictionary.Cards.Count(), 3);
    }

    [TestMethod]
    public void DeleteDicReturnsString()
    {
      var mock = new Mock<IDictionaryService>();
      mock.Setup(x => x.Delete(5)).Returns(true);

      DictionariesController controller = new DictionariesController(mock.Object);

      // Act
      string result = controller.Delete(5) as string;

      Assert.AreEqual(result, HtmlUtils.Successful);
      mock.Verify(x => x.Delete(5), Times.Once());
    }
  }
}
