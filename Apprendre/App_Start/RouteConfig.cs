﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Apprendre
{
  public class RouteConfig
  {
    public static void RegisterRoutes(RouteCollection routes)
    {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

      routes.LowercaseUrls = true;

      routes.MapRoute(
          "UserView",
          "User/{id}",
          new { controller = "User", action = "View", id = UrlParameter.Optional },
          new { id = @"\d+" }
      );

      routes.MapRoute(
          "User",
          "User",
          new { controller = "User", action = "View"}
      );

      routes.MapRoute(
          "UserProfile",
          "User/{id}/{action}/{dicid}/{isdirect}",
          new { controller = "User", action = "View" },
          new { id = @"\d+" }
      );


      routes.MapRoute(
         "Study",
         "",
         new { controller = "Study", action = "Main" }
     );


      routes.MapRoute(
          "Dictionaries",
          "Dictionaries/{action}/{id}",
          new { controller = "Dictionaries", action = "List", id = UrlParameter.Optional }
      );


      routes.MapRoute(
          name: "Default",
          url: "{controller}/{action}/{id}",
          defaults: new { controller = "User", action = "View", id = UrlParameter.Optional }
      );

    }
  }

  public class HyphenatedRouteHandler : MvcRouteHandler
  {
    protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
      requestContext.RouteData.Values["controller"] = requestContext.RouteData.Values["controller"].ToString().Replace("-", "");
      requestContext.RouteData.Values["action"] = requestContext.RouteData.Values["action"].ToString().Replace("-", "");
      return base.GetHttpHandler(requestContext);
    }
  }
}
