﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Apprendre.Models;
using System.Data.Entity;
using System.Web.Script.Serialization;
using Apprendre.Resources;
using Microsoft.AspNet.Identity.Owin;
using Apprendre.Filters;

namespace Apprendre.Controllers
{  
  public abstract class BaseController : Controller
  {
    private AppUser curentUser;
    public AppUser CurrentUser // core user / results
    {
      get
      {
        if (curentUser == null && User.Identity.IsAuthenticated)
        {
          var userName = User.Identity.Name;
          curentUser = new AppContext().Users.Include(x => x.CurrentDictionary).FirstOrDefault(x => x.UserName == userName);          
        }
        return curentUser;
      }
    }

   protected RedirectToRouteResult RedirectHome()
    {
      return RedirectToAction("Main", "Study");
    }

    protected void SetCookie(string Name, string Value)
    {
      HttpCookie cookie = Request.Cookies[Name];
      if (cookie != null) cookie.Value = Value;
      else
      {
        cookie = new HttpCookie(Name, Value)
        {
          HttpOnly = false,
          Expires = DateTime.Now.AddYears(3)
        };
      }
      Response.Cookies.Add(cookie);
    }
  }
}