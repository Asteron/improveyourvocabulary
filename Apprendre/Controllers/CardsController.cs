﻿using Apprendre.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Apprendre.Filters;

namespace Apprendre.Controllers
{
  [Authorize(Roles = "moderator")]
  public class CardsController : BaseController
  {
    private ICardService CardSV;

    public CardsController(ICardService cs)
    {
      CardSV = cs;
    }

    [SetCurDicStat]
    public ActionResult Edit(int id)
    {
      Card card = CardSV.Get(id);
      if (card == null) return HttpNotFound("Invalid id");
      return View(card);
    }

    public PartialViewResult EditContainer(Card card)
    {
      return PartialView(card);
    }

    [HttpPost]
    public string Edit(CardEditVM card)
    {
      if (ModelState.IsValid)
      {
        if (CardSV.Update(card, User.Identity.Name)) return HtmlUtils.Successful;
      }
      return HtmlUtils.Error;
    }

    [SetCurDicStat]
    public ActionResult Marked()
    {
      var marked = CardSV.GetMarked();
      return View(marked);
    }

    [SetCurDicStat]
    public ActionResult Add(int? id, int? added)
    {
      ViewBag.Added = added;

      int selected = id ?? -1;

      var dics = CardSV.GetAvailableDictionaries();
      var selDics = dics.Select(x => new SelectListItem() { Text = x.Name, 
        Value = x.Id.ToString(), Selected = x.Id == selected }).ToList();
      return View(selDics);
    }

    [HttpPost]
    public ActionResult Add(int dicId, string cardsContent)
    {
      if (string.IsNullOrWhiteSpace(cardsContent))
        return View("Error", new ErrorVM(Resources.Resource.eContentIsEmpty));

      var added = CardSV.AddCards(dicId, cardsContent);
      if (added == -1) 
        return View("Error", new ErrorVM("invalid dictionary id"));

      else if (added == 0) 
        return View("Error", new ErrorVM(Resources.Resource.eContentIsEmpty));

      return RedirectToAction("Add", "Cards", new { added = added, Id = dicId });
    }

    [AllowAnonymous]
    [SetCurDicStat]
    public ActionResult View(int id)
    {
      var card = CardSV.Get(id);
      if (card == null) return HttpNotFound("Invalid id");
      return View(card);
    }

    
         
    [HttpPost]
    public string Delete(int id)
    {
      bool result = CardSV.Delete(id);
      return result ? HtmlUtils.Successful : HtmlUtils.Error;
    }


    [Authorize(Roles = "user")]
    [HttpPost]
    public string Mark(int cardId)
    {
      bool result = CardSV.Mark(cardId, User.Identity.Name);
      return result ? HtmlUtils.Successful : HtmlUtils.Error;
    }
    
    
    [HttpPost]
    public ActionResult Unmark()
    {
      CardSV.UnmarkAll();
      return Redirect(Request.UrlReferrer.ToString());
    }

    [HttpPost]
    public JsonResult Search(int dicId, string text)
    {
      IEnumerable<Card> Cards = CardSV.Search(dicId, text);
      return Json(Cards);
    }
  }
}