﻿using Apprendre.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Threading;
using System.Globalization;
using Apprendre.Resources;

namespace Apprendre.Controllers
{  
  public class CoreController : BaseController
  {
    protected ICoreService CoreSV { get; set; }

    public CoreController(ICoreService cs)
    {
      CoreSV = cs;
    }

    [HttpPost]
    public JsonResult GetCards(int? dicId)
    {
      if (User.Identity.IsAuthenticated)
      {
        return Json(CoreSV.GetCards());
      }
      else if (dicId.HasValue)
      {
        var results = Session["results"] as List<Result>;
        var cardsToShow = CoreSV.GetCardsAnonym(dicId.Value, true, ref results);
        Session["results"] = results;
        return Json(cardsToShow);
      }
      return null;
    }
    
    [HttpPost]
    public JsonResult SendResult(int cardId, bool isDirect, int result)
    {
      if (User.Identity.IsAuthenticated)
      {
        LastResult lastResult = null;
        int dicId = 0;
        if (result == -1) dicId = CoreSV.Skip(cardId, isDirect, CurrentUser.Id);
        else
        {
          lastResult = CoreSV.SendResult(cardId, isDirect, result, CurrentUser.Id);
          dicId = lastResult.DicId;
        }

        Mode mode = new Mode(dicId, isDirect);

        DicStat dicStat = CoreSV.GetDicStat(CurrentUser.Id, mode);
        if (CurrentUser.MixedModeActive) dicStat.IsInMix = true;

        if (User.IsInRole("admin")) dicStat.LastResult = lastResult;      
        return Json(dicStat);
      }
      else
      {
        var results = Session["results"] as List<Result>;
        if (results == null) return null;
        CoreSV.SendResultAnonym(cardId, isDirect, result, ref results);
        Session["results"] = results;
      }
      return null;
    }




    public JsonResult GetChooseCards(int? dicId)
    {
      if (User.Identity.IsAuthenticated)
      {
        return Json(CoreSV.GetChooseCards());
      }
      else if (dicId.HasValue)
      {
        return Json(CoreSV.GetChooseCardsAnonym(dicId.Value, true));
      }

      return null;
    }

    [Authorize]
    [HttpPost]
    public JsonResult SendChooseResult(int CardId, bool IsDirect, bool IsCorrect, int Time)
    {
      LastResult lastResult = CoreSV.SendChooseResult(CardId, IsDirect, IsCorrect, Time, CurrentUser.Id);
      Mode mode = new Mode(lastResult.DicId, IsDirect);
      DicStat dicStat = CoreSV.GetDicStat(CurrentUser.Id, mode);
      if (CurrentUser.MixedModeActive) dicStat.IsInMix = true;

      if (User.IsInRole("admin"))
      {
        dicStat.LastResult = lastResult;
      }
      return Json(dicStat);
    }




    [Authorize]
    [HttpPost]
    public JsonResult GetDicStat()
    {
      Mode firstMode = CurrentUser.GetFirstMode();
      DicStat dicStat = CoreSV.GetDicStat(CurrentUser.Id, firstMode);
      if (CurrentUser.MixedModeActive)
      {
        dicStat.IsInMix = true;
        dicStat.DicChooseAvailable = true;
      }
      return Json(dicStat);
    }

    [Authorize]
    [HttpPost]
    public JsonResult GetModes()
    {
      List<DicStat> sumStat = CoreSV.GetSumStat(CurrentUser) as List<DicStat>;

      foreach (var d in sumStat)
      {
        d.DicName += (d.IsDirect ? "" : " (" + Resource._InverseShort + ")") + "   " + d.Old + " + " + d.WeakReady + " / " + d.Studied + " / " + d.Studying;
      }

      var inMix = sumStat.Where(x => x.IsInMix);
      if (inMix.Count() > 1)
      {
        DicStat mixed = new DicStat
        {
          DicId = -999,
          DicName = Resource.uSeveral + "   " + inMix.Sum(x => x.Old) + " + " + inMix.Sum(x => x.WeakReady) + " / " + inMix.Sum(x => x.Studied) + " + " + inMix.Sum(x => x.Studying),
          IsDirect = true,
          IsCurrent = CurrentUser.MixedModeActive
        };
        sumStat.Insert(0, mixed);
      }
      
      var modes = sumStat.Select(x => new NamedMode
      {
        DicId = x.DicId,
        DicName = x.DicName,
        IsDirect = x.IsDirect,
        IsCurrent = x.IsCurrent
      });
      
      return Json(modes);
    }
    


   
  }
}