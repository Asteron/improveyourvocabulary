﻿using Apprendre.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Text;
using System.Web.Hosting;
using Apprendre.Filters;

namespace Apprendre.Controllers
{
  public class DictionariesController : BaseController
  {
    private IDictionaryService DicSV;

    public DictionariesController(IDictionaryService ds)
    {
      DicSV = ds;
    }

    [SetCurDicStat]
    public ActionResult List()
    {
      var allDictionaries = DicSV.GetAllDictionariesVM();
      return View(allDictionaries);      
    }

    [SetCurDicStat]
    public ActionResult View(DictionaryRV rv, int? added)
    {
      ViewBag.Added = added;

      int pageSize = 100;
      DictionaryVM vm = DicSV.GetDictionary(rv, pageSize);

      if (vm == null) return RedirectToAction("List");
      return View(vm);      
    }

    [Authorize(Roles = "moderator")]
    [SetCurDicStat]
    public ActionResult Add(int? added)
    {
      ViewBag.Added = added;
      ViewBag.Languages = DicSV.GetAllDicLanguages();
      return View();
    }

    [HttpPost]
    [Authorize(Roles = "moderator")]
    public ActionResult Add(DictionaryAddVM dictionary)
    {
      if (!ModelState.IsValid) return View(dictionary);

      int dicId = DicSV.Add(dictionary, CurrentUser.Id, out int added);
      if (dicId == -1)
      {
        ModelState.AddModelError("", Resources.Resource.eDictionaryAlreadyExists);
        return View(dictionary);
      }
      
      return RedirectToAction("View", new { id = dicId, added = added });
    }

    [HttpPost]
    [Authorize(Roles = "admin")]
    public string Delete(int id)
    {
      bool isSuccessful = DicSV.Delete(id);
      return isSuccessful ? HtmlUtils.Successful : HtmlUtils.Error;
    }
    
    [HttpPost]
    [Authorize(Roles = "moderator")]
    public string Edit(DictionaryEditVM dictionary)
    {
      if (ModelState.IsValid)
      {
        var result = DicSV.Edit(dictionary);
        if (result) return HtmlUtils.Successful;
      }      
      return HtmlUtils.Error;      
    }

  }
}