﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Apprendre.Models;
using System.Web.UI.WebControls;
using System.Diagnostics;
using Apprendre.Filters;

namespace Apprendre.Controllers
{
  [Authorize(Roles="moderator")]
  public class ResultsController : BaseController
  {
    IResultService ResultSV;
    public ResultsController(IResultService rs)
    {
      ResultSV = rs;
    }

    private readonly double ServerTimezone = double.Parse(System.Configuration.ConfigurationManager.AppSettings["ServerTimezone"]);

    [Authorize(Roles = "admin")]
    [SetCurDicStat]
    public ActionResult Edit(int Id)
    {
      Result result = ResultSV.GetResult(Id);
      if (result == null) return RedirectHome();

      result.ToLocalTime(CurrentUser.Timezone, ServerTimezone);
      return View(result);
    }

    [Authorize(Roles = "admin")]
    [HttpPost]
    public string Edit(ResultEditVM Result)
    {
      if (ModelState.IsValid)
      {
        Result.ToServerTime(CurrentUser.Timezone, ServerTimezone);
        bool isSuccessful = ResultSV.Update(Result);
        if (isSuccessful) return HtmlUtils.Successful;
      }
      return HtmlUtils.Error;
    }

    [Authorize(Roles = "admin")]
    [HttpPost]
    public string Delete(int Id)
    {
      return ResultSV.Delete(Id) ? HtmlUtils.Successful : HtmlUtils.Error;
    }

    [Authorize(Roles = "admin")]
    [HttpPost]
    public ActionResult DeleteAll(int UserId, int DicId, bool IsDirect)
    {
      ResultSV.DeleteAll(UserId, DicId, IsDirect);

      return RedirectToAction("View", "User", new { Id = UserId, dic = DicId, isDirect = IsDirect, Message = UserController.Message.StatisticsDeletedSuccess });
    }




    [SetCurDicStat]
    public ActionResult View(ResultsRV rv)
    {
      int pageSize = 100;
      var vm = ResultSV.GetResults(rv, pageSize);
      foreach (var r in vm.Results)
        r.ToLocalTime(CurrentUser.Timezone, ServerTimezone);
      return View(vm);
    }

    [SetCurDicStat]
    public ActionResult Nulls()
    {
      return RedirectToAction("View", new ResultsRV { OnlyNulls = true });
    }

    [SetCurDicStat]
    public ActionResult Reset()
    {
      int[] cnt = ResultSV.ResetDateTime();
      return View("Error", new ErrorVM(string.Format("removed {0} null results, reset date in {1} results", cnt[0], cnt[1])));
    }


    [AllowAnonymous]
    public ActionResult Guest(ResultsRV rv)
    {
      var results = Session["results"] as List<Result>;
      if (results == null) results = new List<Result>();
      int pageSize = 100;

      ResultsVM vm = new ResultsVM()
      {
        RouteValues = rv,
        PageInfo = new PageInfo(1, pageSize, results.Count),
        Results = results.ToArray()
      };
            
      return View("View", vm);
    }

  }
}