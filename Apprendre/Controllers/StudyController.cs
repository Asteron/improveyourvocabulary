﻿
using Apprendre.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Apprendre.Filters;
using Apprendre.Resources;

namespace Apprendre.Controllers
{
  
  public class StudyController : BaseController
  {
    private IDictionaryService DicSV;

    public StudyController(IDictionaryService ds)
    {
      DicSV = ds;
    }

    [SetCurDicStat]
    public ActionResult Main(string Message, int? Try)
    {
      ViewBag.IsMain = true;

      if (Try.HasValue) ViewBag.DicTrySet = true;

      if (Session["ConfirmationSent"] as bool? == true)
      {
        ViewBag.Message = Resource.uConfirmationSent;
        Session["ConfirmationSent"] = null;
      }
      
      StudyVM vm = new StudyVM()
      {
        IsAuthenticated = User.Identity.IsAuthenticated,
        Training2Available = true,
        IsDicTrySet = Try.HasValue
      };

      if (ViewBag.CurDicStat is DicStat CurDicStat 
        && !CurDicStat.IsInMix
        && (!CurDicStat.DicChooseAvailable || CurDicStat.Studied < 10))
        vm.Training2Available = false;

      if (!User.Identity.IsAuthenticated)
      {
        int defaultDicId = 2;
        if (Try.HasValue)
        {
          var cookie = Request.Cookies.Get("guestdic");
          if (cookie != null) int.TryParse(cookie.Value, out defaultDicId);
          defaultDicId = Try.Value;
          SetCookie("guestdic", Try.Value.ToString());
        }
        vm.Dictionaries = DicSV.GetAllDictionaries().Select(x => new SelectListItem() { Text = x.Name, Value = x.Id.ToString(), Selected = x.Id == defaultDicId }).OrderBy(x => x.Text).ToList();
      }     
      return View(vm);
    }

  }
}