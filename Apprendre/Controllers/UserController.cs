﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Apprendre.Models;
using System.Data.Entity;
using System.Web.Script.Serialization;
using Apprendre.Resources;
using Microsoft.AspNet.Identity.Owin;
using Apprendre.Filters;
using System.Threading;
using System.Collections;

namespace Apprendre.Controllers
{
  public class UserController : BaseController
  {
    public UserController(
      IUserService us, ICoreService cs, IDictionaryService ds, IStatService ss)
    {
      CoreSV = cs;
      UserSV = us;
      DictionarySV = ds;
      StatSV = ss;
    }
    private IUserService UserSV;
    private IDictionaryService DictionarySV;
    private ICoreService CoreSV;
    private IStatService StatSV;

    private readonly double ServerTimezone = double.Parse(System.Configuration.ConfigurationManager.AppSettings["ServerTimezone"]);

    private AppUserManager _userManager;
    private AppUserManager UserManager
    {
      get
      {
        if (_userManager == null)
        {
          _userManager = new AppUserManager(new AppUserStore(new AppContext()));
          var provider = new MachineKeyProtectionProvider();
          UserManager.UserTokenProvider = new DataProtectorTokenProvider<AppUser, int>(provider.Create("ResetPasswordPurpose"));
        }
        return _userManager;
      }
    }

    [SetCurDicStat]
    public ActionResult View(int? id, Message? message)
    {
      if (!id.HasValue && !User.Identity.IsAuthenticated)
        return RedirectHome();

      ViewBag.Message =
           message == Message.ConfirmEmailSuccess ? Resource.uConfirmEmailSuccess
         : message == Message.StatisticsDeletedSuccess ? Resource.uStatisticsDeleted
         : "";

      int userId = id ?? User.Identity.GetUserId<int>();
      AppUser pageUser = UserManager.Users.Include(x => x.CurrentDictionary).FirstOrDefault(x => x.Id == userId);
      if (pageUser == null) return RedirectHome();

      Mode firstMode = pageUser.GetFirstMode();
      var SumStat = CoreSV.GetSumStat(pageUser);
      var CurDicStat = SumStat.FirstOrDefault(x => x.DicId == firstMode.DicId && x.IsDirect == firstMode.IsDirect);

      UserVM VM = new UserVM()
      {
        AppUser = pageUser,
        UserId = pageUser.Id,
        UserName = pageUser.UserName,
        SumStat = SumStat,
        CurDicStat = CurDicStat,
        DicId = CurDicStat.DicId,
        DicName = CurDicStat.DicName,
        IsDirect = CurDicStat.IsDirect
      };

      IEnumerable<Dictionary> dictionaries = DictionarySV.GetAllDictionaries();
      VM.Dictionaries = dictionaries.Select(x => new SelectListItem() { Text = x.Name, Value = x.Id.ToString(), Selected = x.Id == pageUser.CurrentDictionary.Id }).OrderBy(x => x.Text).ToList();

      VM.IsOwner = userId == User.Identity.GetUserId<int>();
      if (VM.IsOwner)
      {
        VM.CurrentModeSettings = UserSV.GetModeSettings(userId, pageUser.CurrentDictionary.Id, pageUser.CurrentIsDirect);

        VM.MixedModeList = UserSV.GetMixedModeSettings(pageUser.Id);
      }

      if (User.IsInRole("admin"))
      {
        VM.LastResults = StatSV.GetLastResults(userId, 20);

        foreach (var r in VM.LastResults)
          r.ToLocalTime(CurrentUser.Timezone, ServerTimezone);

        VM.MaxCntResults = StatSV.GetMaxCntResults(userId, 20);

        foreach (var r in VM.MaxCntResults)
          r.ToLocalTime(CurrentUser.Timezone, ServerTimezone);
      }
      return View(VM);
    }


    [SetCurDicStat]
    public ActionResult Stat(int id, int dicId, bool isDirect)
    {
      AppUser pageUser = UserManager.Users.Include(x => x.CurrentDictionary).FirstOrDefault(x => x.Id == id);
      if (pageUser == null) return RedirectHome();

     
      if (!DictionarySV.Exists(dicId)) return RedirectHome();
      DicStat ds = CoreSV.GetDicStat(id, new Mode(dicId, isDirect));

      double[][] strengthRanksData = StatSV.GetStrengthRanksData(id, dicId, isDirect);
      string json = new JavaScriptSerializer().Serialize(strengthRanksData);

      string daysData = StatSV.GetDaysData(id, dicId, isDirect);

      var weakResults = StatSV.GetWeakestResults(id, dicId, isDirect, 30);

      UserStatVM VM = new UserStatVM
      {
        UserId = id,
        UserName = pageUser.UserName,
        DicId = ds.DicId,
        DicName = ds.DicName,
        IsDirect = ds.IsDirect,
        DicStat = ds,
        RanksData = json,
        DaysData = daysData,
        WeakResults = weakResults, 
        IsOwner = id == CurrentUser.Id
      };
      return View("Stat", VM);
     
    }


    [Authorize]
    [SetCurDicStat]
    public ActionResult Dictionary(UserDictionaryRV rv)
    {
      if (CurrentUser.Id != rv.Id && !User.IsInRole("moderator"))
      {
        return RedirectToAction("View", new { Id = rv.Id });
      }

      int pageSize = 100;
      var dicResultsVM = StatSV.GetDicResults(rv, pageSize);
      dicResultsVM.IsOwner = rv.Id == CurrentUser.Id;
      return View(dicResultsVM);
    }

    [HttpPost]
    public string SkipCard(int cardId, bool isDirect, bool skip)
    {
      return StatSV.SkipCard(CurrentUser.Id, cardId, isDirect, skip) ?
        HtmlUtils.Successful : HtmlUtils.Error;
    }






    //
    // SETTINGS & MODE
    //

    public enum Message
    {
      ChangePasswordSuccess,
      WrongOldPasswordError,
      ChangeEmailSuccess,
      ChangeEmailError,
      ConfirmEmailSuccess,
      StatisticsDeletedSuccess
    }

    [Authorize]
    [SetCurDicStat]
    public ActionResult Settings(Message? message)
    {
      ViewBag.Message =
            message == Message.ChangePasswordSuccess ? Resource.uPasswordChanged
          : message == Message.WrongOldPasswordError ? Resource.uWrongOldPassword
          : message == Message.ChangeEmailSuccess ? Resource.uEmailChanged
          : message == Message.ChangeEmailError ? Resource.uChangeEmailError
          : message == Message.ConfirmEmailSuccess ? Resource.uConfirmEmailSuccess
          : "";

      string Language = Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;
      ViewBag.languagesSL = SiteLanguages.List.Select(x => new SelectListItem() { Value = x.Code, Text = x.SelfName, Selected = x.Code == Language });


      ViewBag.timezonesSL = Enumerable.Range(-12, 27).Select(x => new SelectListItem
      {
        Value = x.ToString(),
        Text = "UTC" + (x >= 0 ? "+" : "") + x,
        Selected = x == CurrentUser.Timezone
      });

      var model = new SettingsVM() 
      {
        Email = CurrentUser.Email, 
        Timezone = CurrentUser.Timezone, 
        Language = CurrentUser.Language 
      };
      return View(model);
    }

    [Authorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> ChangeSettings(SettingsVM model)
    {
      object RouteValues = null;
      if (ModelState.IsValid)
      {
        if (CurrentUser.Email != model.Email)
        {
          await SendConfirmation(CurrentUser);
          RouteValues = new { Message = Message.ChangeEmailSuccess };
        }       

        if (SiteLanguages.List.Any(x => x.Code == model.Language))
          SetLanguageCookie(model.Language);
        else model.Language = null;

        UserSV.ChangeSettings(CurrentUser.Id, 
          model.Email, 
          model.Timezone, 
          model.Language);

        SetFontSizeCookie(model.FontSize);
      }
      return RedirectToAction("Settings", RouteValues);
    }

    private void SetFontSizeCookie(int? fontSize)
    {
      HttpCookie cookie = Request.Cookies["fontSize"];

      if (fontSize == null || fontSize.Value <= 10 || fontSize.Value == 100 || fontSize > 1000)
      {
        if (cookie != null) cookie.Expires = DateTime.Now.AddYears(-1);
      }
      else
      {
        if (cookie != null) cookie.Value = fontSize.ToString();
        else
        {
          cookie = new HttpCookie("fontSize", fontSize.ToString())
          {
            HttpOnly = false,
            Expires = DateTime.Now.AddYears(3)
          };
        }
      }
      if (cookie != null) Response.Cookies.Add(cookie);
    }

    private void SetLanguageCookie(string language)
    {
      SetCookie("language", language);
    }


    [Authorize]
    [HttpPost]
    public JsonResult GetModeSettings(int dicId, bool isDirect)
    {
      ModeSetting dus = UserSV.GetModeSettings(CurrentUser.Id, dicId, isDirect);

      var obj = new { dus.StudyLimit, dus.RandomOrder };
      return Json(obj);
    }

    [Authorize]
    [HttpPost]
    public ActionResult ChangeMode(int dicId, bool isDirect, bool? needRedirect, int? limit, bool? randomOrder)
    {
      if (DictionarySV.Exists(dicId) == false || 
        UserSV.ChangeMode(CurrentUser.Id, dicId, isDirect, limit, randomOrder) == false)
      {
        return new ContentResult() { Content = HtmlUtils.Error };
      }
        
      if (needRedirect == true)
        return new ContentResult() { Content = HtmlUtils.Successful };

      return Redirect(Request.UrlReferrer.ToString());
    }


    [Authorize]
    [HttpPost]
    public string ChangeMixedMode(ModeSettingEditVM[] modes)
    {
      if (modes == null 
        || DictionarySV.Exists(modes.Select(x => x.DicId)) == false 
        || UserSV.ChangeMixedMode(CurrentUser.Id, modes) == false)
      {
        return HtmlUtils.Error;
      }
      return HtmlUtils.Successful;
    }

    [Authorize]
    [HttpPost]
    public string SwitchToMixedMode()
    {
      if (!UserSV.SwitchToMixedMode(CurrentUser.Id))
      {
        return HtmlUtils.Error;
      }
      return HtmlUtils.Successful;
    }














    //
    // AUTHORIZATION
    //

    [Authorize]
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> ChangePassword(ChangePasswordVM model)
    {
      ViewBag.ReturnUrl = Url.Action("Settings");

      if (ModelState.IsValid)
      {
        IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId<int>(), model.OldPassword, model.NewPassword);
        if (result.Succeeded)
        {
          return RedirectToAction("Settings", new { Message = Message.ChangePasswordSuccess });
        }
        else AddErrors(result);
      }
      return RedirectToAction("Settings", new { Message = Message.WrongOldPasswordError });
    }












    public ActionResult Reset_Password()
    {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Reset_Password(ResetPasswordVM model)
    {
      if (ModelState.IsValid)
      {
        AppUser user = UserManager.FindByEmail(model.Email);
        if (user == null)
        {
          ModelState.AddModelError("", Resource.uNoUserWithThisEmail);          
        }
        else
        {
          string token = UserManager.GeneratePasswordResetToken(user.Id);
          string callbackUrl = Url.Action("SetNewPassword", "User", new { userId = user.Id, token = token }, protocol: Request.Url.Scheme);

          string subject = Resource.uEmailTitlePasswordReset;
          string body = Resource.uEmailBodyPasswordReset + "<p><a href=\"" + callbackUrl + "\">" + callbackUrl + "</a></p>";
          await UserManager.SendEmailAsync(user.Id, subject, body);
          return View("ResetMessageSent");
        }
      }
      return View(model);
    }


    public ActionResult SetNewPassword(int userId, string token)
    {
      AppUser user = UserManager.FindById(userId);
      if (user == null) return View("Error", new ErrorVM(Resource.uWrongLink));

      return View(new SetNewPasswordVM { UserId = userId, Token = token });
    }

    
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> SetNewPassword(SetNewPasswordVM model)
    {
      ViewBag.ReturnUrl = Url.Action("Settings");

      if (!ModelState.IsValid) return View("Error", new ErrorVM(Resource.uWrongData));

      IdentityResult result = await UserManager.ResetPasswordAsync(model.UserId, model.Token, model.NewPassword);
      if (result.Succeeded)
      {
        var user = UserManager.FindById(model.UserId);
        await SignInAsync(user, true);
        SetLanguageCookie(user.Language); 
      }

      return RedirectHome();
    }











    [AllowAnonymous]
    [HttpPost]
    public string ChangeLanguage(string language)
    {
      return ChangeTheLanguage(language) ? HtmlUtils.Successful : HtmlUtils.Error;
    }

    private bool ChangeTheLanguage(string language)
    {
      if (SiteLanguages.List.FirstOrDefault(x => x.Code == language) != null)
      {
        SetLanguageCookie(language);
        return true;
      }
      return false;
    }


        
    public ActionResult Register()
    {
      return View();
    }
    



    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Register(RegisterVM model)
    {
      if (ModelState.IsValid)
      {
        int stDicId = 2;       
        var cookie = Request.Cookies.Get("guestdic");
        if (cookie != null) int.TryParse(cookie.Value, out stDicId); 

        AppUser user = new AppUser()
        {
          UserName = model.UserName,
          LastLogged = DateTime.Now,
          CurrentDictionaryId = stDicId,
          CurrentIsDirect = true,
          Email = model.Email,
          EmailConfirmed = false,
          Language = model.Language,
          Timezone = 2
        };
        var result = await UserManager.CreateAsync(user, model.Password);
        if (result.Succeeded)
        {
          UserManager.AddToRole(user.Id, "user");

          int id = user.Id;
          var results = Session["results"] as List<Result>;
          if (results != null && results.Count > 0)
            UserSV.SaveUnsavedResults(results, user.Id);
          Session["results"] = null;

          await SignInAsync(user, isPersistent: false);

          await SendConfirmation(user);
          

          Session["ConfirmationSent"] = true;
          return RedirectHome();
        }
        else AddErrors(result);
      }
      return View(model);
    }

    private async Task SendConfirmation(AppUser user)
    {
      string token = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
      string callbackUrl = Url.Action("Confirm_Email", "User", new { userId = user.Id, token = token }, protocol: Request.Url.Scheme);

      string subject = Resource.uEmailTitleConfirm;
      string body = Resource.uEmailBodyConfirm + "<p><a href=\"" + callbackUrl + "\">" + callbackUrl + "</a></p>";
      await UserManager.SendEmailAsync(user.Id, subject, body);
    }

    public ActionResult Confirm_Email(int userId, string token)
    {
      AppUser user = UserManager.FindById(userId);
      if (user == null) return View("Error", new ErrorVM(Resource.uWrongConfCode));

      var result = UserManager.ConfirmEmail(user.Id, token);
      if (result.Succeeded)
      {
        return RedirectToAction("View", new { Message = Message.ConfirmEmailSuccess });
      }
      return View("Error", new ErrorVM(Resource.uWrongConfCode));
    }


    public ActionResult Login(string returnUrl)
    {
      ViewBag.ReturnUrl = returnUrl;
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Login(LoginVM model, string returnUrl)
    {
      if (ModelState.IsValid)
      {
        var user = await UserManager.FindAsync(model.UserName, model.Password);
        if (user != null)
        {
          await SignInAsync(user, model.RememberMe);
          SetLanguageCookie(user.Language);

          if (Url.IsLocalUrl(returnUrl)) return Redirect(returnUrl);
          else return RedirectHome();
        }
        else
        {
          ModelState.AddModelError("", Resource.uInvalidUNOrPassword);
        }
      }
      return View(model);
    }

    private async Task SignInAsync(AppUser user, bool isPersistent)
    {
      AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
      ClaimsIdentity identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

      // Extend identity claims
      identity.AddClaim(new Claim(ClaimTypes.Sid, user.Id.ToString()));

      AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
    }


    [HttpPost]
    [Authorize]
    public ActionResult Logout()
    {
      AuthenticationManager.SignOut();
      return RedirectHome();
    }


    [Authorize(Roles="admin")]
    [SetCurDicStat]
    public ActionResult All_Users()
    {
      var users = UserSV.GetAllUsers();

      double difference = CurrentUser.Timezone - ServerTimezone;
      foreach (var user in users)
      {
        if (user.LastActivity.HasValue)
            user.LastActivity = user.LastActivity.Value.AddHours(difference);
      }

      List<AppUserVM> VM = new List<AppUserVM>();
      foreach (var u in users)
      {
        AppUserVM auVM = new AppUserVM() { AppUser = u };
        auVM.Roles = UserManager.GetRoles(u.Id);
        if (!u.MixedModeActive) auVM.CurrentModes = new List<NamedMode>() {
          new NamedMode
        {
          DicId = u.CurrentDictionary.Id,
          DicName = u.CurrentDictionary.Name,
          IsDirect = u.CurrentIsDirect }
        };
        else
        {
          var modeList = u.GetMixedModeList();
          int[] dicIds = modeList.Select(x => x.DicId).Distinct().ToArray();
          var dics = DictionarySV.GetDictionaries(dicIds);
          var namedModes = new List<NamedMode>();
          foreach(var mode in modeList)
          {
            var dic = dics.FirstOrDefault(x => x.Id == mode.DicId);
            if (dic == null) continue;
            namedModes.Add(new NamedMode { DicId = mode.DicId, IsDirect = mode.IsDirect, DicName = dic.Name });
          }
          auVM.CurrentModes = namedModes;
        }
        VM.Add(auVM);
      }
      return View(VM);
    }






    


    private IAuthenticationManager AuthenticationManager
    {
      get
      {
        return HttpContext.GetOwinContext().Authentication;
      }
    }
    
    private void AddErrors(IdentityResult result)
    {
      foreach (var error in result.Errors)
      {
        ModelState.AddModelError("", error);
      }
    }
    

  }
}