﻿using Apprendre.Controllers;
using Apprendre.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Apprendre.Filters
{
  public class SetCurDicStatAttribute : FilterAttribute, IActionFilter
  {
    public void OnActionExecuted(ActionExecutedContext context)
    { }

    public void OnActionExecuting (ActionExecutingContext context)
    {
      if (context.HttpContext.User.Identity.IsAuthenticated)
      {
        var controller = context.Controller as BaseController;
        AppUser user = controller.CurrentUser;
        Mode firstMode = user.GetFirstMode();
        
        DicStat ds = new CoreService().GetDicStat(user.Id, firstMode);
        if (user.MixedModeActive) ds.IsInMix = true;
        context.Controller.ViewBag.CurDicStat = ds;

        string language = user.Language;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(language);
        Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(language);

        HttpCookie cookie = context.RequestContext.HttpContext.Request.Cookies["language"];
        if (cookie == null || cookie.Value != language)
        {
          var newCookie = new HttpCookie("language", language)
          {
            HttpOnly = false,
            Expires = DateTime.Now.AddYears(3)
          };
          context.RequestContext.HttpContext.Response.Cookies.Add(newCookie);
        }
      }
    }

  }
}