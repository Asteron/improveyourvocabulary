﻿using Apprendre.Models;
using Apprendre.Utils;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Apprendre
{
  public class MvcApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);

      NinjectModule registrations = new NinjectRegistrations();
      IKernel kernel = new StandardKernel(registrations);
      DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
    }


    public void Application_OnBeginRequest(object sender, EventArgs e)
    {
      string cultureName = SiteLanguages.List[0].Code;

      var cultureCookie = (sender as MvcApplication).Request.Cookies["language"];
      if (cultureCookie != null) cultureName = cultureCookie.Value;

      if (SiteLanguages.List.FirstOrDefault(x => x.Code == cultureName) == null)
      {
        cultureName = SiteLanguages.List[0].Code;
      }
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);
    }
  }
}
    
