﻿using Apprendre.Models;
using System;
using System.Text;
using System.Web;
using System.Web.Mvc;

public static class PagingHelpers
{
  public static MvcHtmlString PageLinks(
    this HtmlHelper html,
    PageInfo pageInfo, 
    string viewName,
    PageableRV rv)
  {
    int pageNumber = pageInfo.PageNumber;
    int totalPages = pageInfo.TotalPages;

    if (pageNumber < 1 || totalPages <= 1)
    {
      return null;
    }

    UrlHelper urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
    StringBuilder sb = new StringBuilder();

    if (totalPages <= 9)
    {
      for (int i = 1; i <= totalPages; i++)
        sb.Append(GetTagString(pageNumber, viewName, rv, i, urlHelper));     
    }
    else
    {
      sb.Append(GetTagString(pageNumber, viewName, rv, 1, urlHelper));
      if (pageNumber >= 1 + 4) sb.Append(GetEllipsis());
      for (int i = 2; i <= totalPages - 1; i++)
      {
        if (
          (i >= pageNumber - 2 && i <= pageNumber + 2)
          || (pageNumber <= 2 && i <= 5)
          || (pageNumber >= totalPages - 1 && i >= totalPages - 4)
          )
        {
          sb.Append(GetTagString(pageNumber, viewName, rv, i, urlHelper));
        }
      }
      if (pageNumber <= totalPages - 4) sb.Append(GetEllipsis());
      sb.Append(GetTagString(pageNumber, viewName, rv, totalPages, urlHelper));
    }
    TagBuilder div = new TagBuilder("div");
    div.AddCssClass("page-group");
    div.InnerHtml = sb.ToString();

    return MvcHtmlString.Create(div.ToString());
  }
   private static string GetTagString(int pageNumber, string viewName, PageableRV rv, int i, UrlHelper urlHelper)
  {
    rv.Page = i;
    string url = urlHelper.Action(viewName, rv);

    TagBuilder a = new TagBuilder("a");
    a.MergeAttribute("href", url);
    a.InnerHtml = i.ToString();

    if (i == pageNumber)
    {
      a.AddCssClass("page-selected");
    }
    a.AddCssClass("page");
    return a.ToString();
  }

  private static string GetEllipsis()
  {
    TagBuilder span = new TagBuilder("span");    
    span.InnerHtml = "...";    
    span.AddCssClass("ellipsis");
    return span.ToString();
  }
}