namespace Apprendre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ModeSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AppUserId = c.Int(nullable: false),
                        DictionaryId = c.Int(nullable: false),
                        IsDirect = c.Boolean(nullable: false),
                        StudyLimit = c.Int(nullable: false),
                        RandomOrder = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.AppUserId, cascadeDelete: true)
                .ForeignKey("dbo.Dictionaries", t => t.DictionaryId, cascadeDelete: true)
                .Index(t => t.AppUserId)
                .Index(t => t.DictionaryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ModeSettings", "DictionaryId", "dbo.Dictionaries");
            DropForeignKey("dbo.ModeSettings", "AppUserId", "dbo.AspNetUsers");
            DropIndex("dbo.ModeSettings", new[] { "DictionaryId" });
            DropIndex("dbo.ModeSettings", new[] { "AppUserId" });
            DropTable("dbo.ModeSettings");
        }
    }
}
