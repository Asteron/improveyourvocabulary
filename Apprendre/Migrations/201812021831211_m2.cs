namespace Apprendre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Dictionaries", "StudyLanguage");
            DropColumn("dbo.Dictionaries", "StydyFromLanguage");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dictionaries", "StydyFromLanguage", c => c.String());
            AddColumn("dbo.Dictionaries", "StudyLanguage", c => c.String());
        }
    }
}
