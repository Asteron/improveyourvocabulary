namespace Apprendre.Migrations
{
  using System;
  using System.Data.Entity.Migrations;

  public partial class m3 : DbMigration
  {
    public override void Up()
    {
      //DropForeignKey("dbo.AspNetUsers", "CurrentDictionaryId", "dbo.Dictionaries"); // Added
      //DropIndex("dbo.AspNetUsers", new[] { "CurrentDictionaryId" });
      //CreateIndex("dbo.AspNetUsers", "CurrentDictionaryId");
      //AddForeignKey("dbo.AspNetUsers", "CurrentDictionaryId", "dbo.Dictionaries", "Id", cascadeDelete: false); // Added

      DropForeignKey("dbo.Cards", "DictionaryId", "dbo.Dictionaries");
      DropIndex("dbo.Cards", new[] { "DictionaryId" });
      //AddColumn("dbo.Dictionaries", "AuthorId", c => c.Int(nullable: false));
      RenameColumn("dbo.Dictionaries", "UserId", "AuthorId"); // Added
      AddColumn("dbo.AspNetUsers", "MixedModeActive", c => c.Boolean(nullable: false));
      AddColumn("dbo.AspNetUsers", "MixedModeSer", c => c.String());
      AlterColumn("dbo.Cards", "DictionaryId", c => c.Int(nullable: false));
      CreateIndex("dbo.Cards", "DictionaryId");
      CreateIndex("dbo.Dictionaries", "AuthorId");

      AddForeignKey("dbo.Dictionaries", "AuthorId", "dbo.AspNetUsers", "Id", cascadeDelete: false); // added cascadeDelete: false

      AddForeignKey("dbo.Cards", "DictionaryId", "dbo.Dictionaries", "Id", cascadeDelete: false);
      //DropColumn("dbo.Dictionaries", "UserId");
      
    }

    public override void Down()
    {
      //AddColumn("dbo.Dictionaries", "UserId", c => c.Int(nullable: false));
      DropForeignKey("dbo.Cards", "DictionaryId", "dbo.Dictionaries");
      DropForeignKey("dbo.Dictionaries", "AuthorId", "dbo.AspNetUsers");
      DropIndex("dbo.Dictionaries", new[] { "AuthorId" });
      DropIndex("dbo.Cards", new[] { "DictionaryId" });
      AlterColumn("dbo.Cards", "DictionaryId", c => c.Int());
      DropColumn("dbo.AspNetUsers", "MixedModeSer");
      DropColumn("dbo.AspNetUsers", "MixedModeActive");
      //DropColumn("dbo.Dictionaries", "AuthorId");
      RenameColumn("dbo.Dictionaries", "AuthorId", "UserId"); // Added
      CreateIndex("dbo.Cards", "DictionaryId");
      AddForeignKey("dbo.Cards", "DictionaryId", "dbo.Dictionaries", "Id");
    }
  }
}
