﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class AppContext : IdentityDbContext<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>
  {
    public AppContext()
      : base("ApprendreDB")
    {
      Database.SetInitializer<AppContext>(new AppDbInitializer());
    }

    public DbSet<Dictionary> Dictionaries { get; set; }
    public DbSet<Card> Cards { get; set; }
    public DbSet<Result> Results { get; set; }
    public DbSet<Skip> Skips { get; set; }
    public DbSet<Mark> Marks { get; set; }
    public DbSet<ModeSetting> ModeSettings { get; set; }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      modelBuilder.Entity<Dictionary>()
        .HasRequired(u => u.Author)
        .WithMany()
        .HasForeignKey(u => u.AuthorId).WillCascadeOnDelete(false);

      modelBuilder.Entity<AppUser>()
        .HasOptional(d => d.CurrentDictionary)
        .WithMany()
        .HasForeignKey(d => d.CurrentDictionaryId).WillCascadeOnDelete(false);
    }
  }
}