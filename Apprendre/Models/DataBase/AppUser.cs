﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;
using Newtonsoft.Json;

namespace Apprendre.Models
{
  public class AppUser : IdentityUser<int, AppUserLogin, AppUserRole, AppUserClaim>, IUser<int>
  {
    public DateTime LastLogged { get; set; }
    
    public Dictionary CurrentDictionary { get; set; }
    public int? CurrentDictionaryId { get; set; }

    public bool MixedModeActive { get; set; }
    public string MixedModeSer { get; set; }

    public bool CurrentIsDirect { get; set; }
    public string Language { get; set; }
    public double Timezone { get; set; }
    public DateTime? LastActivity { get; set; }

    public List<Result> Results { get; set; }
    public List<Skip> Skips { get; set; }
    public List<Mark> Marks { get; set; }

    public List<Mode> GetMixedModeList()
    {
      if (MixedModeSer == null || MixedModeSer.Length == 0) return new List<Mode>();

      int[][] mml = JsonConvert.DeserializeObject<int[][]>(MixedModeSer);
      foreach(var x in mml)
      {
        var mode = new Mode(x[0], x[1] == 1);
      }
      return mml.Select(x => new Mode(x[0], x[1] == 1)).ToList();        
    }

    public Mode GetFirstMode()
    {
      int dicId = CurrentDictionary.Id;
      bool isDirect = CurrentIsDirect;

      if (MixedModeActive)
      {
        var mml = GetMixedModeList();
        if (mml.Count >= 1)
        {
          dicId = mml[0].DicId;
          isDirect = mml[0].IsDirect;
        }
      }
      return new Mode (dicId, isDirect);
    }

    //public List<Mode> GetCurrentModes()
    //{
    //  if (!MixedModeActive) 
    //}
  }  
}