﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Apprendre.Models
{
  public class Card : ICard
  {
    public int Id { get; set; }

    [Required]
    public Dictionary Dictionary { get; set; }
    public int DictionaryId { get; set; }

    [Required]
    [StringLength(100)]
    public string Word { get; set; }

    [Required]
    [StringLength(200)]
    public string Translation { get; set; }

    [StringLength(100)]
    public string Transcription { get; set; }

    [StringLength(500)]
    public string Example { get; set; }

    public List<Result> Results { get; set; }

    public List<Skip> Skips { get; set; }


    public List<Mark> Marks { get; set; }
  }
}