﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class Dictionary : IDictionary
  {
    public int Id { get; set; }

    [Required]
    [StringLength(70)]
    [DataType(DataType.Text)]
    public string Name { get; set; }
    public List<Card> Cards { get; set; }

    [Required]
    public AppUser Author { get; set; }
    public int AuthorId { get; set; }

    public bool IsPublic { get; set; }
    public string WordLanguage { get; set; }
    public string TranslationLanguage { get; set; }
    public bool ChooseExerciseAvailable { get; set; }
  }
}