﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class Mark
  {
    public int Id { get; set; }

    [Required]
    public AppUser User { get; set; }
    public int UserId { get; set; }

    [Required]
    public Card Card { get; set; }
    public int CardId { get; set; }

    public string Comment { get; set; }
  }
}