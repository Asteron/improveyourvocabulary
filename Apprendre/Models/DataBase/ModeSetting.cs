﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class ModeSetting
  {
    public int Id { get; set; }

    [Required]
    public AppUser AppUser { get; set; }
    public int AppUserId { get; set; }

    [Required]
    public Dictionary Dictionary { get; set; }
    public int DictionaryId { get; set; }

    public bool IsDirect { get; set; }

    public int StudyLimit { get; set; }

    public bool RandomOrder { get; set; }
  }
}