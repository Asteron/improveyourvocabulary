﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class Result : IResult
  {
    public int Id { get; set; }

    [Required]
    public AppUser User { get; set; }
    public int UserId { get; set; }

    [Required]
    public Card Card { get; set; }
    public int CardId { get; set; }

    public bool IsDirect { get; set; }

    [DataType(DataType.DateTime)]
    public DateTime FirstTime { get; set; }

    [DataType(DataType.DateTime)]
    public DateTime Showed { get; set; }

    [DataType(DataType.DateTime)]
    public DateTime? Updated { get; set; }

    public int UpdatedCnt { get; set; }

    public double? Strength { get; set; }    
  }
}