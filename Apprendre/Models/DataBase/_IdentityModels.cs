﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Apprendre.Models
{
  public class AppUserLogin : IdentityUserLogin<int> { }

  public class AppUserClaim : IdentityUserClaim<int> { }

  public class AppUserRole : IdentityUserRole<int> { }

  public class AppRole : IdentityRole<int, AppUserRole> { }

  public static class AppRoles
  {
    public const string Admin = "admin";
    public const string Moderator = "moderator";
    public const string User = "user";
  }

  public class AppUserStore : UserStore<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>, IUserStore<AppUser, int>
  {
    public AppUserStore(AppContext context) : base(context) { }
  }

  public class AppUserManager : UserManager<AppUser, int>
  {
    public AppUserManager(IUserStore<AppUser, int> store)
      : base(store)
    {
      this.EmailService = new SmtpEmailService();

      UserValidator = new UserValidator<AppUser, int>(this)
      {
        RequireUniqueEmail = true,
        AllowOnlyAlphanumericUserNames = false
      };

      PasswordValidator = new PasswordValidator
      {
        RequiredLength = 6
      };

    }
  }


  public class AppRoleStore : RoleStore<AppRole, int, AppUserRole>
  {
    public AppRoleStore(AppContext context) : base(context) { }
  }
  public class AppRoleManager : RoleManager<AppRole, int>
  {
    public AppRoleManager(IRoleStore<AppRole, int> store) : base(store) { }
  }







  public class MachineKeyProtectionProvider : IDataProtectionProvider
  {
    public IDataProtector Create(params string[] purposes)
    {
      return new MachineKeyDataProtector(purposes);
    }
  }

  public class MachineKeyDataProtector : IDataProtector
  {
    private readonly string[] _purposes;

    public MachineKeyDataProtector(string[] purposes)
    {
      _purposes = purposes;
    }

    public byte[] Protect(byte[] userData)
    {
      return MachineKey.Protect(userData, _purposes);
    }

    public byte[] Unprotect(byte[] protectedData)
    {
      return MachineKey.Unprotect(protectedData, _purposes);
    }
  }
}