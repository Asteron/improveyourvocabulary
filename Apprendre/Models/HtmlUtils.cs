﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Apprendre.Models
{
  public static class HtmlUtils
  {
    public const string Successful = "successful";
    public const string Error = "error";


    public static string RenderViewToString(ControllerContext context, string viewName, object model)
    {
      if (string.IsNullOrEmpty(viewName))
        viewName = context.RouteData.GetRequiredString("action");

      var viewData = new ViewDataDictionary(model);

      using (var sw = new StringWriter())
      {
        var viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
        var viewContext = new ViewContext(context, viewResult.View, viewData, new TempDataDictionary(), sw);
        viewResult.View.Render(viewContext, sw);

        return sw.GetStringBuilder().ToString();
      }
    }

    public static string InsertLinks(this string text, params string[] links)
    {
      return InsertLinksWithClass(text, null, links);
    }

    public static string InsertLinksWithClass(this string text, string linkClass, params string[] links)
    {
      string NewText = text;
      string _class = linkClass == null ? "" : " class=\"" + linkClass + "\"";

      foreach (string link in links)
      {
        NewText = new Regex("\\{").Replace(NewText, "<a href='" + link + "'" + _class + ">", 1);
        NewText = new Regex("\\}").Replace(NewText, "</a>", 1);
      }
      return NewText;
    }

  }
}