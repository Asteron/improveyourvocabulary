﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Hosting;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace Apprendre.Models
{
  public class AppDbInitializer : CreateDatabaseIfNotExists<AppContext>
  {
    protected override void Seed(AppContext ac)
    {
      var userManager = new AppUserManager(new AppUserStore(ac));
      var roleManager = new AppRoleManager(new AppRoleStore(ac));

      roleManager.Create(new AppRole { Name = AppRoles.Admin });
      roleManager.Create(new AppRole { Name = AppRoles.Moderator });
      roleManager.Create(new AppRole { Name = AppRoles.User });

      var user1 = new AppUser()
      {
        UserName = "Phemmer",
        Email = "example@yandex.ru",
        LastLogged = DateTime.Now,
        CurrentIsDirect = true,
        //CurrentDictionary = ac.Dictionaries.First(x => x.Id == 4),
        Language = SiteLanguages.List[0].Code,
        Timezone = 2
      };
      var user2 = new AppUser()
      {
        UserName = "Irena",
        Email = "example@example.ru",
        LastLogged = DateTime.Now,
        CurrentIsDirect = true,
        //CurrentDictionary = ac.Dictionaries.First(x => x.Id == 2),
        Language = SiteLanguages.List[0].Code,
        Timezone = 2
      };
          
      IdentityResult create1 = userManager.Create(user1, "111111");
      if (create1.Succeeded)
      {
        userManager.AddToRole(user1.Id, AppRoles.Admin);
        userManager.AddToRole(user1.Id, AppRoles.Moderator);
        userManager.AddToRole(user1.Id, AppRoles.User);
      }
      IdentityResult create2 = userManager.Create(user2, "111111");
      if (create2.Succeeded)
      {
        userManager.AddToRole(user2.Id, AppRoles.Moderator);
        userManager.AddToRole(user2.Id, AppRoles.User);
      }



      var fileContents = File.ReadAllText(HostingEnvironment.MapPath(@"~/Content/id4123190_words.sql"), Encoding.UTF8);

      string command1 = Regex.Match(fileContents, "SET IDENTITY_INSERT \"dictionaries\" ON ;[\\s\\S]+SET IDENTITY_INSERT \"dictionaries\" OFF").Value
        .Replace(") VALUES", ", \"authorid\", \"ispublic\", \"ChooseExerciseAvailable\") VALUES")
        .Replace(", '", ", N'")
        .Replace("')", "', 1, 'true', 'true')");
      ac.Database.ExecuteSqlCommand(command1);

      string command2 = Regex.Match(fileContents, "SET IDENTITY_INSERT \"words\" ON ;[\\s\\S]+SET IDENTITY_INSERT \"words\" OFF").Value
        .Replace(", 0, NULL)", ", NULL)")
        .Replace(", 1, NULL)", ", NULL)")
        .Replace(", NULL, 0,", ", NULL,")
        .Replace(", NULL, 1,", ", NULL,")
        .Replace("dic_id", "dictionaryid")
        .Replace(" \"mark\",", "")
        .Replace(", '", ", N'")
        .Replace("\\'", "''")
        .Replace("\"words\"", "\"cards\"")
        .Replace("\\n", "\n");

      ac.Database.ExecuteSqlCommand(command2);


      string command3 = Regex.Match(fileContents, "SET IDENTITY_INSERT \"results\" ON ;[\\s\\S]+SET IDENTITY_INSERT \"results\" OFF").Value
        .Replace("user_id", "userid")
        .Replace("word_id", "cardid")
        .Replace("is_direct", "isDirect")
        .Replace("first_time", "firstTime")
        .Replace("updated_cnt", "updatedCnt");
      ac.Database.ExecuteSqlCommand(command3);

      user1.CurrentDictionaryId = 2;
      user2.CurrentDictionaryId = 1;
      ac.SaveChanges();

      base.Seed(ac);
    }
  }
}