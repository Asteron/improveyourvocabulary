﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public interface ICard
  {
   string Word { get; set; }
   string Translation { get; set; }
   string Transcription { get; set; }
   string Example { get; set; }
  }
}