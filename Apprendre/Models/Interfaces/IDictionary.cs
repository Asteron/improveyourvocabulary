﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public interface IDictionary
  {
    string Name { get; set; }
    string WordLanguage { get; set; }
    string TranslationLanguage { get; set; }
    bool ChooseExerciseAvailable { get; set; }
  }
}