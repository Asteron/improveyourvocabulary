﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public interface IResult
  {
    int Id { get; set; }
    
    DateTime FirstTime { get; set; }

    DateTime Showed { get; set; }

    DateTime? Updated { get; set; }

    int UpdatedCnt { get; set; }

    double? Strength { get; set; }
  }
}