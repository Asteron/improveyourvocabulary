﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public static class ResultHelpers
  {

    public static void ToLocalTime(this IResult result, double UserTimezone, double ServerTimezone)
    {
      double difference = UserTimezone - ServerTimezone;
      result.FirstTime = result.FirstTime.AddHours(difference);
      result.Showed = result.Showed.AddHours(difference);

      if (result.Updated.HasValue)
        result.Updated = result.Updated.Value.AddHours(difference);
    }

    public static void ToServerTime(this IResult result, double UserTimezone, double ServerTimezone)
    {
      double difference = -UserTimezone + ServerTimezone;
      result.FirstTime = result.FirstTime.AddHours(difference);
      result.Showed = result.Showed.AddHours(difference);

      if (result.Updated.HasValue)
        result.Updated = result.Updated.Value.AddHours(difference);
    }
  }
}