﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{  
  public class DictionaryRV : PageableRV
  {
    public int Id { get; set; }

    public DictionaryRV Clone(int order)
    {
      DictionaryRV clone = this.MemberwiseClone() as DictionaryRV;
      clone.Order = order;
      clone.Desc = Order == order ? !(this.Desc == true) : false;
      clone.Page = 1;
      return clone;
    }
  }
}