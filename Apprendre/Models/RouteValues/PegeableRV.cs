﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public abstract class PageableRV
  {
    public int? Page { get; set; }

    public int? Order { get; set; }
    public bool? Desc { get; set; }

    public string OrderSign(int order)
    {
      return Order != order ? "" : Desc == true ? "▼" : "▲";
    }

  }
}