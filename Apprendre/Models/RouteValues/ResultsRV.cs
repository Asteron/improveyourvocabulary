﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class ResultsRV : PageableRV
  {
    public int? UserId { get; set; }

    public int? DicId { get; set; }
    public bool? IsDirect { get; set; }

    public bool? OnlyNulls { get; set; }

    public ResultsRV Clone(int order)
    {
      ResultsRV clone = this.MemberwiseClone() as ResultsRV;
      clone.Order = order;
      clone.Desc = Order == order ? !(this.Desc == true) : order >= 5 && order <=8;
      clone.Page = 1;
      return clone;
    }
  }

  
}