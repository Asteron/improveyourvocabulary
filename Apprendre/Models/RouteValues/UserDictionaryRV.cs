﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class UserDictionaryRV : PageableRV
  {
    public int Id { get; set; }

    public int DicId { get; set; }
    public bool IsDirect { get; set; }

    public UserDictionaryRV Clone(int order)
    {
      UserDictionaryRV clone = this.MemberwiseClone() as UserDictionaryRV;
      clone.Order = order;
      clone.Desc = Order == order ? !(this.Desc == true) : false;
      clone.Page = 1;
      return clone;
    }    
  }
}