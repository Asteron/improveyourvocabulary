﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Apprendre.Models
{
  public interface ICardService
  {
    Card Get(int Id);
    IEnumerable<Mark> GetMarked();
    int AddCards(int DicId, string CardsContent);
    bool Update(CardEditVM Card, string UserName);
    bool Delete(int Id);
    bool Mark(int CardId, string UserName);
    bool Unmark(int CardId);
    int UnmarkAll();
    IEnumerable<Dictionary> GetAvailableDictionaries();
    IEnumerable<Card> Search(int dicId, string text);
  }
  public class CardService : ICardService
  {
    private AppContext ac = new AppContext();

    public Card Get(int Id)
    {
      return ac.Cards.Include(x => x.Dictionary).FirstOrDefault(x => x.Id == Id);
    }

    public IEnumerable<Mark> GetMarked()
    {
      return ac.Marks.Include(x => x.Card).Include(x => x.Card.Dictionary).Include(x => x.User).OrderBy(x => x.Card.Dictionary.Name).ThenBy(x => x.Card.Word).ToArray();
    }



    public int AddCards(int DicId, string CardsContent)
    {
      Dictionary dic = ac.Dictionaries.FirstOrDefault(x => x.Id == DicId);
      if (dic == null) return -1;

      string[] lines = CardsContent.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
      List<Card> cards = new List<Card>();
      foreach (var line in lines)
      {
        var spl = line.Split(new char[] { '\t', '=' });
        if (spl.Length < 2) continue;

        string word = spl[0];
        string translation = spl[1];
        if (string.IsNullOrWhiteSpace(word)
          || string.IsNullOrWhiteSpace(translation)) continue;

        Card card = new Card()
        {
          Word = word.Trim(),
          Translation = translation.Trim(),
          Dictionary = dic
        };
        if (spl.Length >= 3) card.Transcription = spl[2];
        if (spl.Length >= 4) card.Example = spl[3];
        cards.Add(card);
      }
      ac.Cards.AddRange(cards);
      return ac.SaveChanges();
    }

    public bool Delete(int Id)
    {
      var results = ac.Results.Include(x => x.Card).Where(x => x.Card.Id == Id);
      ac.Results.RemoveRange(results);
      var cnt2 = ac.SaveChanges();

      var marks = ac.Marks.Include(x => x.Card).Where(x => x.Card.Id == Id);
      ac.Marks.RemoveRange(marks);
      var cnt3 = ac.SaveChanges();

      Card card = ac.Cards.FirstOrDefault(x => x.Id == Id);
      if (card == null) return false;
      var ccc = ac.Cards.Where(x => x.Id == Id).ToList();
      ac.Cards.Remove(card);
      var cnt = ac.SaveChanges();
      return true;
    }

    public bool Update(CardEditVM Card, string UserName)
    {
      Card card = ac.Cards.FirstOrDefault(x => x.Id == Card.Id);
      if (card == null) return false;

      card.Word = Card.Word;
      card.Translation = Card.Translation;
      card.Transcription = Card.Transcription;
      card.Example = Card.Example;
      ac.SaveChanges();

      if (Card.Marked) Mark(Card.Id, UserName);
      else Unmark(Card.Id);
      return true;
    }

    public bool Mark(int CardId, string UserName)
    {
      var card = ac.Cards.FirstOrDefault(x => x.Id == CardId);
      if (card == null) return false;

      var mark = ac.Marks.Include(x => x.Card).FirstOrDefault(x => x.Card.Id == card.Id);
      if (mark == null)
      {
        AppUser user = ac.Users.FirstOrDefault(x => x.UserName == UserName);
        mark = new Mark() { Card = card, User = user }; 
        ac.Marks.Add(mark);
        ac.SaveChanges();
      }
      return true;
    }

    public bool Unmark(int CardId)
    {
      var marks = ac.Marks.Include(x => x.Card).Where(x => x.Card.Id == CardId);
      ac.Marks.RemoveRange(marks);
      int removed = ac.SaveChanges();
      return removed > 0;
    }

    public int UnmarkAll()
    {
      var marked = ac.Marks;
      ac.Marks.RemoveRange(marked);
      return ac.SaveChanges();
    }
    
    public IEnumerable<Dictionary> GetAvailableDictionaries()
    {
      return ac.Dictionaries.OrderBy(x => x.Name).ToArray();
    }

    public IEnumerable<Card> Search(int dicId, string text)
    {
      var cards = ac.Cards.Where(x => x.DictionaryId == dicId
      && (x.Word.Contains(text)
          || x.Translation.Contains(text)
          || x.Transcription.Contains(text))).OrderByDescending(x => x.Word == text || x.Translation == text).ThenByDescending(x => x.Word.StartsWith(text) || x.Translation.StartsWith(text)).ThenBy(x => x.Word).Take(50);
      return cards.AsEnumerable();
    }
  }
}