﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Text;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;

namespace Apprendre.Models
{
  public interface ICoreService
  {
    CardsToShow GetCards();
    int Skip(int cardId, bool isDirect, int id);
    LastResult SendResult(int cardId, bool isDirect, int result, int userId);

    IEnumerable<MiniCard[]> GetChooseCards();
    LastResult SendChooseResult(int cardId, bool isDirect, bool isCorrect, int time, int userId);

    CardsToShow GetCardsAnonym(int dicId, bool isDirec, ref List<Result> results);
    void SendResultAnonym(int cardId, bool isDirect, int result, ref List<Result> results);

    IEnumerable<MiniCard[]> GetChooseCardsAnonym(int dicId, bool isDirect);
    LastResult SendChooseResultAnonym(int cardId, bool isDirect, bool isCorrect, int time, int userId);

    DicStat GetDicStat(int userId, Mode mode);
    IEnumerable<DicStat> GetSumStat(AppUser user);
  }

  public class CoreService : ICoreService
  {
    private AppContext ac = new AppContext();

    private static readonly double S035 = 0.35;
    private static readonly double S050 = 0.5;
    private static readonly double S070 = 0.7;
    private static readonly double RANMAXSTRENGTH = 0.75;

    private static readonly int OLDUPDATEDCNT = 6;

    private static readonly int OLDINTERVAL = 2 * 24 * 3600;
    private static readonly int OLDINTERVAL2 = 30 * 24 * 3600;

    private static readonly double MK1 = 0.5 * 86400;
    private static readonly double MK2 = 800 * 86400;
    private static readonly double MK3 = 6;

    private static readonly double MINTSHOWED = 5 * 60;

    private static readonly double MINT = 0 * 60;
    private static readonly double MAXT = 260 * 60;
    private static readonly double DELTA = 0.01;
    private static readonly double EXP = 2;

    public CardsToShow GetCards()
    {
      string userName = HttpContext.Current.User.Identity.Name;
      AppUser user = ac.Users.Include(x => x.CurrentDictionary).FirstOrDefault(x => x.UserName == userName);

      if (user == null) return null;
      if (!user.MixedModeActive)
        return GetCardsByMode(user, user.CurrentDictionary.Id, user.CurrentIsDirect);

      Mode ds = GetProportionalRandomMode(user);
      return GetCardsByMode(user, ds.DicId, ds.IsDirect);
    }

    private Mode GetProportionalRandomMode(AppUser user)
    {
      var sumStat = GetSumStat(user);
      var mixedModeList = user.GetMixedModeList();
      List<DicStat> mixedStats = new List<DicStat>();
      foreach (var mm in mixedModeList)
      {
        var stat = sumStat.FirstOrDefault(x => x.DicId == mm.DicId && x.IsDirect == mm.IsDirect);
        if (stat != null) mixedStats.Add(stat);
        else
        {
          if (ac.Dictionaries.Any(x => x.Id == mm.DicId))
            mixedStats.Add(new DicStat { DicId = mm.DicId, IsDirect = mm.IsDirect });
        }
      }

      if (mixedStats.Count == 0) return null;

      var counts = mixedStats.Select(x => x.Old + x.WeakReady).ToArray();
      var accum = new int[counts.Length];
      for (int i = 0; i < accum.Length; i++)
      {
        counts[i] += 1;
        for (int j = 0; j <= i; j++)
        {
          accum[i] += counts[j];
        }
      }

      Random ran = new Random();
      int ranValue = ran.Next(accum[accum.Length - 1]);
      int index = Array.FindIndex(accum, x => x >= ranValue);
      DicStat ds = mixedStats[index];
      return ds;
    }

    private CardsToShow GetCardsByMode(AppUser user, int dicId, bool isDirect)
    {     
      int userId = user.Id;
      bool isAdmin = HttpContext.Current.User.IsInRole("admin");

      int TargetWordsCnt = 5;

      ModeSetting modeSetting = ac.ModeSettings.FirstOrDefault(x => 
        x.AppUserId == userId && x.DictionaryId == dicId && x.IsDirect == isDirect);
      if (modeSetting == null) modeSetting = new ModeSetting() { AppUserId = userId, DictionaryId = dicId, IsDirect = isDirect };

      DateTime Now = DateTime.Now;
      Random random = new Random();
      List<SortableCard> sortableCards = new List<SortableCard>();
      //Stopwatch sw = new Stopwatch(); sw.Start();

      // Count of existing results
      var stat = ac.Results.Include(x => x.User).Include(x => x.Card).Include(x => x.Card.Dictionary)
        .Where(x => x.Strength.HasValue && x.User.Id == userId && x.Card.Dictionary.Id == dicId && x.IsDirect == isDirect)
        .GroupBy(x => x.IsDirect)
        .Select(x => new
        {
          studied = x.Count(),

          weak = x.Where(y => y.Strength < S035).Count(),

          weakReady = x.Where(y => y.Strength < S035 && DbFunctions.DiffSeconds(y.Updated, Now) > MINT + MAXT * Math.Pow(y.Strength.Value + DELTA, EXP)).Count(),

          oldGood = x.Where(y => y.Strength >= S035
            && y.UpdatedCnt >= OLDUPDATEDCNT
            && DbFunctions.DiffSeconds(y.Updated.Value, Now) > OLDINTERVAL
            && (y.Strength < 0.8 || DbFunctions.DiffSeconds(y.Updated, Now) > OLDINTERVAL2)
            && DbFunctions.DiffSeconds(y.Updated, Now) > DbFunctions.DiffSeconds(y.FirstTime, y.Updated)).Count(),

          oldMid = x.Where(y => y.Strength >= S035
            && (y.UpdatedCnt < OLDUPDATEDCNT || y.Strength < S050)
            && DbFunctions.DiffSeconds(y.Updated, Now) > MK1 + MK2 * Math.Pow(y.Strength.Value, MK3)).Count()
        }).FirstOrDefault();

      int Weak_DB = 0;
      int WeakReady_DB = 0;
      int oldSumCnt = 0;
      int Studied_DB = 0;

      if (stat != null)
      {
        Studied_DB = stat.studied;
        WeakReady_DB = stat.weakReady;
        Weak_DB = stat.weak;
        int OldCnt = 0;
        int OldGood = 0;
        int OldMid = 0;

        if (WeakReady_DB < 10 && Weak_DB < 40) OldCnt = 3 + random.Next(2);
        else if (WeakReady_DB < 15 && Weak_DB < 45) OldCnt = 3;
        else if (WeakReady_DB < 20 && Weak_DB < 50) OldCnt = 2 + random.Next(2);
        else if (WeakReady_DB < 25 && Weak_DB < 55) OldCnt = 2;
        else if (WeakReady_DB < 30 && Weak_DB < 55) OldCnt = 1 + random.Next(2);
        else if (WeakReady_DB < 40 && Weak_DB < 60) OldCnt = 1;
        else if (WeakReady_DB < 45 && Weak_DB < 60) OldCnt = random.Next(2);

        oldSumCnt = stat.oldGood + stat.oldMid;
        if (oldSumCnt > 0)
        {
          double oldRatio = (double)stat.oldGood / oldSumCnt;
          OldGood = (int)Math.Round(oldRatio * OldCnt);
          OldMid = OldCnt - OldGood;
        }

        // 1
        // 0.5 Old by difficulty
        int OldGoodCnt = random.Next(2);
        int RandomCnt = 1 - OldGoodCnt;

        if (OldGoodCnt > 0 && Weak_DB <= 65)
        {
          var mid3Cards = ac.Results.Include(x => x.User).Include(x => x.Card).Where(x => x.Card.Dictionary.Id == dicId
            && x.IsDirect == isDirect
            && x.User.Id == userId
            && x.Strength >= S035
            && DbFunctions.DiffSeconds(x.Showed, Now) > MINTSHOWED
            && (MK1 + MK2 * Math.Pow(x.Strength.Value, MK3)) / DbFunctions.DiffSeconds(x.Updated, Now) < 3)
            .OrderBy(x => (MK1 + MK2 * Math.Pow(x.Strength.Value, MK3)) / DbFunctions.DiffSeconds(x.Updated, Now)).Take(OldGoodCnt);

          foreach (var r in mid3Cards)
          {
            r.Showed = Now;
            double a = (MK1 + MK2 * Math.Pow(r.Strength.Value, MK3)) / (Now - r.Updated.Value).TotalSeconds;
            sortableCards.Add(new SortableCard(r, "mid3_" + a.ToString("F1")));
          }
          ac.SaveChanges();
          
        }

        // 0.5 Random  
        if (RandomCnt > 0 && Weak_DB <= 50)
        {
          var randomCard = ac.Results.Include(x => x.User).Include(x => x.Card).Where(x => x.Card.Dictionary.Id == dicId && x.IsDirect == isDirect && x.User.Id == userId && x.Strength < RANMAXSTRENGTH
            && DbFunctions.DiffSeconds(x.Updated, Now) > MINT + MAXT * Math.Pow(x.Strength.Value * DELTA, EXP)
            && DbFunctions.DiffSeconds(x.Showed, Now) > MINTSHOWED).OrderBy(c => Guid.NewGuid()).Take(RandomCnt); //0.05  
          foreach (var r in randomCard)
          {
            r.Showed = Now;
            sortableCards.Add(new SortableCard(r, "ran"));
          }
          ac.SaveChanges();      
        }

        // 2
        // Old strong (from the beginning of studying)
        if (OldGood > 0)
        {
          var oldCards = ac.Results.Include(x => x.User).Include(x => x.Card).Where(x => x.Card.Dictionary.Id == dicId
            && x.IsDirect == isDirect
            && x.User.Id == userId
            && x.Strength >= S035
            && x.UpdatedCnt >= OLDUPDATEDCNT
            && DbFunctions.DiffSeconds(x.Updated.Value, Now) > OLDINTERVAL
            && (x.Strength < 0.8 || DbFunctions.DiffSeconds(x.Updated, Now) > OLDINTERVAL2)
            && DbFunctions.DiffSeconds(x.Updated, Now) > DbFunctions.DiffSeconds(x.FirstTime, x.Updated)
            && DbFunctions.DiffSeconds(x.Updated, Now) > MINT + MAXT * Math.Pow(x.Strength.Value + DELTA, EXP)
            && DbFunctions.DiffSeconds(x.Showed, Now) > MINTSHOWED)
             .OrderBy(x => DbFunctions.DiffSeconds(x.Updated, Now) - DbFunctions.DiffSeconds(x.FirstTime, x.Updated)).Take(OldGood);

          foreach (var r in oldCards)
          {
            r.Showed = Now;
            sortableCards.Add(new SortableCard(r, "old_g"));
          }
          ac.SaveChanges();
        }

        // Old medium-strength
        if (OldMid > 0)
        {
          var oltCards = ac.Results.Include(x => x.User).Include(x => x.Card).Where(x => x.Card.Dictionary.Id == dicId
            && x.IsDirect == isDirect
            && x.User.Id == userId
            && x.Strength >= S035
            && DbFunctions.DiffSeconds(x.Showed, Now) > MINTSHOWED
            && (x.UpdatedCnt < OLDUPDATEDCNT || x.Strength < S050)
            && (MK1 + MK2 * Math.Pow(x.Strength.Value, MK3)) / DbFunctions.DiffSeconds(x.Updated, Now) < 1)
            .OrderBy(x => (MK1 + MK2 * Math.Pow(x.Strength.Value, MK3)) / DbFunctions.DiffSeconds(x.Updated, Now)).Take(OldMid);

          foreach (var r in oltCards)
          {
            r.Showed = Now;

            double a = (MK1 + MK2 * Math.Pow(r.Strength.Value, MK3)) / (Now - r.Updated.Value).TotalSeconds;
            sortableCards.Add(new SortableCard(r, "old_m" + a.ToString("F1")));
          }
          ac.SaveChanges();
        }
      }

      // New
      if (Weak_DB <= 45 && (modeSetting.StudyLimit == 0 || Studied_DB < modeSetting.StudyLimit))
      {
        int WeakReadyAndOld = WeakReady_DB + oldSumCnt;
        int NewCnt = 0;

        if      (WeakReadyAndOld == 00) NewCnt = 5;
        else if (WeakReadyAndOld <= 02) NewCnt = 3 + random.Next(2);
        else if (WeakReadyAndOld <= 04) NewCnt = 3; // 3n 1w
        else if (WeakReadyAndOld <= 06) NewCnt = 2 + random.Next(2);
        else if (WeakReadyAndOld <= 08) NewCnt = 2; // 2n 2w
        else if (WeakReadyAndOld <= 10) NewCnt = 1 + random.Next(2);
        else if (WeakReadyAndOld <= 15) NewCnt = 1; // 1n 3w
        else if (WeakReadyAndOld <= 25 && random.Next(0, 3) == 0) NewCnt = 1;

        NewCnt = Math.Min(NewCnt, TargetWordsCnt - sortableCards.Count);
        if (modeSetting.StudyLimit > 0 && Studied_DB < modeSetting.StudyLimit)
        {
          int limit = modeSetting.StudyLimit - Studied_DB;
          NewCnt = Math.Min(NewCnt, limit);
        }

        if (NewCnt > 0)
        {
          var cards = ac.Cards.Include(x => x.Dictionary).Include(x => x.Results).Include(x => x.Skips).Where
            (x => x.Dictionary.Id == dicId
            && x.Results.FirstOrDefault(y => y.User.Id == userId
              && y.IsDirect == isDirect
              && (y.Updated.HasValue || DbFunctions.DiffSeconds(y.Showed, Now) < MINTSHOWED)) == null

              && x.Skips.FirstOrDefault(y => y.User.Id == userId
              && y.IsDirect == isDirect) == null
            )
            .Select(x => new { card = x, reverse = x.Results.FirstOrDefault(y => y.User.Id == userId && y.IsDirect == !isDirect && y.Updated.HasValue) });

          if (modeSetting.RandomOrder)
            cards = cards.OrderBy(x => Guid.NewGuid());               
          else
            cards = cards.OrderByDescending(x => x.reverse.Strength);                

          Card[] newCards = cards.Select(x => x.card).Take(NewCnt).ToArray();

          foreach (Card nc in newCards)
          {
            if (nc.Results != null)
            {
              Result res = nc.Results.FirstOrDefault(y => y.User.Id == userId && y.IsDirect == isDirect);
              if (res != null) res.Showed = Now;
            }
            else
            {
              Result res = new Result() { Card = nc, Showed = Now, FirstTime = Now, IsDirect = isDirect, User = user };
              ac.Results.Add(res);
              sortableCards.Add(new SortableCard(res, "new"));
            }
          }
          ac.SaveChanges();
        }
      }

      // Weak (0.27 - 0.35)
      int WeakCnt = TargetWordsCnt - sortableCards.Count;

      int Weak027Cnt = 0;
      if (WeakCnt >= 3) Weak027Cnt = 1;
      if (WeakCnt == 2) Weak027Cnt = random.Next(2);
      double stren = random.Next(21, 27)/100.0;

      if (Weak027Cnt > 0)
      {
        var weak027ReadyCards = ac.Results.Include(x => x.User).Include(x => x.Card).Where(x => x.Card.Dictionary.Id == dicId
          && x.IsDirect == isDirect && x.User.Id == userId
          && x.Strength < S035
          && x.Strength > stren
          && DbFunctions.DiffSeconds(x.Updated, Now) > MINT + MAXT * Math.Pow(x.Strength.Value + DELTA, EXP)
          && DbFunctions.DiffSeconds(x.Showed, Now) > MINTSHOWED).OrderBy(x => x.Strength).ThenBy(x => x.Updated).Take(Weak027Cnt);

        foreach (var r in weak027ReadyCards)
        {
          r.Showed = Now;
          sortableCards.Add(new SortableCard(r, "weak " + stren + "+"));
        }
        ac.SaveChanges();
      }
      // Weak (0 - 0.35)
      WeakCnt = TargetWordsCnt - sortableCards.Count;
      if (WeakCnt > 0)
      {
        var weakReadyCards = ac.Results.Include(x => x.User).Include(x => x.Card).Where(x => x.Card.Dictionary.Id == dicId
          && x.IsDirect == isDirect && x.User.Id == userId
          && x.Strength < S035
          && DbFunctions.DiffSeconds(x.Updated, Now) > MINT + MAXT * Math.Pow(x.Strength.Value + DELTA, EXP)
          && (DbFunctions.DiffSeconds(x.Showed, Now) > MINTSHOWED || 
              DbFunctions.DiffSeconds(x.Showed, x.Updated) > 0))
          .OrderBy(x => x.Strength).ThenBy(x => x.Updated).Take(WeakCnt);

        foreach (var r in weakReadyCards)
        {
          r.Showed = Now;
          sortableCards.Add(new SortableCard(r, "weak"));
        }
        ac.SaveChanges();
      }

      
      // Any
      int AnyCnt = TargetWordsCnt - sortableCards.Count;
      if (AnyCnt > 0)
      {
        List<Result> anyCards = ac.Results.Include(x => x.User).Include(x => x.Card).Where(x => x.Card.Dictionary.Id == dicId && x.IsDirect == isDirect && x.User.Id == userId && x.Strength.HasValue)
          .OrderByDescending(x => x.Updated > x.Showed || DbFunctions.DiffSeconds(x.Showed, Now) > MINTSHOWED)
          .ThenByDescending(x => DbFunctions.DiffSeconds(x.Updated, Now) > MINT + MAXT * Math.Pow(x.Strength.Value + DELTA, EXP))
          .ThenBy(x => x.Strength)
          .ThenBy(x => x.Updated)
          .Take(AnyCnt).ToList();
        foreach (var r in anyCards)
        {
          r.Showed = Now;
          sortableCards.Add(new SortableCard(r, "any"));
        }
        ac.SaveChanges();
      }

      sortableCards = sortableCards.OrderByDescending(x => x.Result.Strength < 0.03).ThenByDescending(x => x.Result.Strength < 0.10).ThenBy(x => Guid.NewGuid()).ToList();

      CardsToShow cardsToShow = new CardsToShow(sortableCards, isAdmin);      
      //sw.Stop();//0.33
      return cardsToShow; 
    }
       
    public int Skip(int cardId, bool isDirect, int userId)
    {
      string userName = HttpContext.Current.User.Identity.Name;
      AppUser CurUser = ac.Users.Include(x => x.CurrentDictionary).FirstOrDefault(x => x.UserName == userName);

      if (CurUser == null) return 0;

      Result Result = ac.Results.FirstOrDefault(x => x.CardId == cardId && x.IsDirect == isDirect && x.UserId == userId);
      if (Result != null) ac.Results.Remove(Result);

      Card card = ac.Cards.FirstOrDefault(x => x.Id == cardId);
      if (card == null) return 0;

      var skips = ac.Skips.Where(x => x.CardId == cardId && x.IsDirect == isDirect && x.UserId == userId);
      if (skips.Count() == 0)
      {
        Skip skip = new Skip { UserId = userId, Card = card, IsDirect = isDirect };
        ac.Skips.Add(skip);
      }
      ac.SaveChanges();
      return card.DictionaryId;
    }

    public LastResult SendResult(int cardId, bool isDirect, int result, int userId)
    {
      Result res = ac.Results.Include(x => x.Card).FirstOrDefault(x => x.CardId == cardId && x.IsDirect == isDirect && x.UserId == userId);
      if (res == null) return null;

      double oldStrength = res.Strength == null ? 0.12 : res.Strength.Value;
      double newStrength = GetNewStrength(result, oldStrength, res.Updated);

      res.Strength = newStrength;
      res.UpdatedCnt++;
      res.Updated = DateTime.Now;

      ac.Users.First(x => x.Id == userId).LastActivity = DateTime.Now;
      ac.SaveChanges();

      return new LastResult
      {
        Id = res.Id,
        OldStrength = oldStrength,
        NewStrength = newStrength,
        DicId = res.Card.DictionaryId
      };
    }

    private double GetNewStrength(int result, double oldStrength, DateTime? updated)
    {
      double newStrength = 0;
      double interval = updated == null ? 60.0 : 
        (DateTime.Now - updated.Value).TotalMinutes;
      double min = 0;
      double max = 0.43;

      if (result == 0)
      {
        newStrength = min + (max - min) * oldStrength; // * 0.43
      }
      else if (oldStrength > 0.05 || interval > 60 * 24)
      {
        double log = Math.Log(interval * 5 / 2.4, 5);
        if (log < 0) log = 0;

        switch (result)
        {
          case 1:
            min = 0.08 + 0.022 * log;
            max = 0.566 - min / 2;
            break;
          case 2:
            min = 0.08 + 0.037 * log;
            max = 1;
            break;
          case 3:
            min = 0.35 + 0.02 * log;
            max = 1;
            break;
        }
        newStrength = min + (max - min) * oldStrength;
      }
      else if (oldStrength < 0.001) newStrength = oldStrength * 8;
      else if (oldStrength < 0.01) newStrength = oldStrength * (result + 2.5);
      else newStrength = oldStrength * (result + 1.5);
      return newStrength;
    }

    public IEnumerable<MiniCard[]> GetChooseCards()
    {
      string userName = HttpContext.Current.User.Identity.Name;
      AppUser user = ac.Users.Include(x => x.CurrentDictionary).FirstOrDefault(x => x.UserName == userName);
      if (user == null) return null;
      if (!user.MixedModeActive)
        return GetChooseCardsByMode(user, user.CurrentDictionary.Id, user.CurrentIsDirect);

      Mode ds = GetProportionalRandomMode(user);
      return GetChooseCardsByMode(user, ds.DicId, ds.IsDirect);
    }

    public IEnumerable<MiniCard[]> GetChooseCardsByMode(AppUser user, int dicId, bool isDirect)
    {
      //Stopwatch sw = new Stopwatch(); sw.Start();      

      DateTime Now = DateTime.Now;
      Random ran = new Random();
      int RanCnt = 2;
      int WeakCnt = 3;
      List<MiniCard[]> miniCards = new List<MiniCard[]>();
      List<MiniCard> _cards = new List<MiniCard>(4);

      var weakResults = ac.Results.Include(x => x.User).Include(x => x.Card).Where(x =>
        x.Card.Dictionary.Id == dicId
        && x.IsDirect == isDirect
        && x.User.Id == user.Id
        && x.Strength.HasValue)
        .OrderByDescending(x => DbFunctions.DiffSeconds(x.Showed, Now) > 5 * 60
          && DbFunctions.DiffSeconds(x.Updated, Now) > MINT + MAXT * Math.Pow(x.Strength.Value + DELTA, EXP))
        .ThenBy(r => r.Strength).Take(WeakCnt * 4).ToArray();

      var ranResults = ac.Results.Include(x => x.User).Include(x => x.Card).Where(x =>
        x.Card.Dictionary.Id == dicId
        && x.IsDirect == isDirect
        && x.User.Id == user.Id
        && x.Strength.HasValue)
        .OrderByDescending(x => DbFunctions.DiffSeconds(x.Showed, Now) > 30 * 60
          && DbFunctions.DiffSeconds(x.Updated, Now) > MINT + MAXT * Math.Pow(x.Strength.Value + DELTA, EXP))
        .ThenBy(r => Guid.NewGuid()).Take(RanCnt * 4).ToArray();

      var results = weakResults.Union(ranResults).OrderBy(x => Guid.NewGuid());
      foreach (var r in results)
      {
        MiniCard mc = new MiniCard
        {
          Id = r.Card.Id,
          Word = r.Card.Word,
          Translation = r.Card.Translation,
          IsDirect = isDirect
        };
        if (_cards.Count == 0) r.Showed = Now;
        _cards.Add(mc);

        if (_cards.Count == 4)
        {
          if (ran.Next(0, 5) <= 1)
          {
            foreach (MiniCard mc1 in _cards)
            {
              var temp = mc1.Translation;
              mc1.Translation = mc1.Word;
              mc1.Word = temp;
              mc1.IsInverse = true;
            }
          }
          miniCards.Add(_cards.ToArray());
          _cards.Clear();
        }
      }
      ac.SaveChanges();
      //sw.Stop();
      return miniCards;
    }

    public LastResult SendChooseResult(int cardId, bool isDirect, bool IsCorrect, int Time, int UserId)
    {
      double oldStrength = 0.12;
      double newStrength = 0;

      double min = 0;
      double max = 0.43;

      Result result = ac.Results.Include(x => x.User).Include(x => x.Card).FirstOrDefault(x => x.Card.Id == cardId && x.IsDirect == isDirect && x.User.Id == UserId);
      if (result != null)
      {
        if (result.Strength.HasValue)
          oldStrength = result.Strength.Value;

        double delta = 0.06 / Math.Pow(Time / 1000.0, 1.1);
        if (delta > 0.2) delta = 0.2;
        if (IsCorrect) newStrength = oldStrength + delta;
        else newStrength = min + (max - min) * oldStrength;  
       
        if (newStrength > 1) newStrength = 1;

        result.Strength = newStrength;
        result.UpdatedCnt++;
        result.Updated = DateTime.Now;

        ac.Users.First(x => x.Id == UserId).LastActivity = DateTime.Now;
        ac.SaveChanges();
      }
      LastResult change = new LastResult
      {
        Id = result.Id,
        OldStrength = oldStrength,
        NewStrength = newStrength,
        DicId = result.Card.DictionaryId
      };
      return change;
    }




    public DicStat GetDicStat(int userId, Mode mode)
    {
      //Stopwatch sw = new Stopwatch(); sw.Start();
      DateTime Now = DateTime.Now;
      int dicId = mode.DicId;
      bool isDirect = mode.IsDirect;
      var dic = ac.Dictionaries.FirstOrDefault(x => x.Id == dicId);

      var stat = ac.Results.Include(x => x.User).Include(x => x.Card).Include(x => x.Card.Dictionary)
        .Where(x => x.Strength.HasValue && x.User.Id == userId && x.Card.DictionaryId == dicId && x.IsDirect == isDirect)
        .GroupBy(x => x.IsDirect)
        .Select(x => new
        {
          skipped = ac.Skips.Count(z => z.Card.DictionaryId == dicId && z.IsDirect == isDirect && z.UserId == userId),

          dicTotal = ac.Cards.Count(z => z.DictionaryId == dicId),
          dicId = dicId,
          dicName = dic.Name,
          dicChooseAvailable = dic.ChooseExerciseAvailable,
          isDirect = isDirect,
          updatedCnt = x.Select(y => y.UpdatedCnt).Sum(),
          studied = x.Count(),
          avgStrength = x.Select(y => y.Strength).Average(),

          weak = x.Where(y => y.Strength < S035).Count(),
          weakReady = x.Where(y => y.Strength < S035 && DbFunctions.DiffSeconds(y.Updated, Now) > MINT + MAXT * Math.Pow(y.Strength.Value + DELTA, EXP)).Count(),

          mid = x.Where(y => y.Strength >= S035 && y.Strength < S050).Count(),
          good = x.Where(y => y.Strength >= S050 && y.Strength < S070).Count(),
          strong = x.Where(y => y.Strength >= S070).Count(),

          old = x.Where(y =>
              (y.Strength >= S035 && y.UpdatedCnt >= OLDUPDATEDCNT
              && DbFunctions.DiffSeconds(y.Updated.Value, Now) > OLDINTERVAL
              && (y.Strength < 0.8 || DbFunctions.DiffSeconds(y.Updated, Now) > OLDINTERVAL2)
              && DbFunctions.DiffSeconds(y.Updated, Now) > DbFunctions.DiffSeconds(y.FirstTime, y.Updated))
            ||
              (y.Strength >= S035 && (y.UpdatedCnt < OLDUPDATEDCNT || y.Strength < S050)
              && DbFunctions.DiffSeconds(y.Updated, Now) > MK1 + MK2 * Math.Pow(y.Strength.Value, MK3))
            ).Count(),

          oldGood = x.Where(y => y.Strength >= S035
            && y.UpdatedCnt >= OLDUPDATEDCNT
            && DbFunctions.DiffSeconds(y.Updated.Value, Now) > OLDINTERVAL
            && (y.Strength < 0.8 || DbFunctions.DiffSeconds(y.Updated, Now) > OLDINTERVAL2)
            && DbFunctions.DiffSeconds(y.Updated, Now) > DbFunctions.DiffSeconds(y.FirstTime, y.Updated)).Count(),

          oldMid = x.Where(y => y.Strength >= S035
            && (y.UpdatedCnt < OLDUPDATEDCNT || y.Strength < S050)
            && DbFunctions.DiffSeconds(y.Updated, Now) > MK1 + MK2 * Math.Pow(y.Strength.Value, MK3)).Count()
        }).FirstOrDefault();

      //sw.Stop();//0.05-0.2
      DicStat DicStat = null;
      if (stat == null)
      {
        var DicTotal = ac.Cards.Count(z => z.DictionaryId == dic.Id);
        var Skipped = ac.Skips.Count(z => z.Card.DictionaryId == dicId && z.IsDirect == isDirect && z.UserId == userId);

        DicStat = new DicStat()
        {
          DicTotal = DicTotal,
          Skipped = Skipped,
          Studying = DicTotal - Skipped,

          UserId = userId,
          DicId = dic.Id,
          DicName = dic.Name,
          IsDirect = isDirect,
        };
      }
      else
      {
        DicStat = new DicStat
        {
          DicTotal = stat.dicTotal,
          Skipped = stat.skipped,
          Studying = stat.dicTotal - stat.skipped,

          AvgStrength = stat.avgStrength.Value,
          Studied = stat.studied,
          UserId = userId,
          DicId = stat.dicId,
          DicName = stat.dicName,
          DicChooseAvailable = stat.dicChooseAvailable,
          Good = stat.good,
          IsDirect = stat.isDirect,
          Mid = stat.mid,
          Old = stat.old,
          OldMid = stat.oldMid,
          OldGood = stat.old - stat.oldMid,
          Strong = stat.strong,
          Weak = stat.weak,
          Updations = stat.updatedCnt,
          WeakReady = stat.weakReady
        }.CountPercentage();
      }
      return DicStat;
    }

    public IEnumerable<DicStat> GetSumStat(AppUser user)
    {
      Stopwatch sw = new Stopwatch(); sw.Start();
      DateTime Now = DateTime.Now;
      
      var stat = ac.Results.Include(x => x.User).Include(x => x.Card).Include(x => x.Card.Dictionary)
        .Where(x => x.Strength.HasValue && x.User.Id == user.Id)
        .GroupBy(x => new { x.Card.Dictionary.Id, x.IsDirect })
        .Select(x => new
        {
          skipped = ac.Skips.Count(z => z.Card.DictionaryId == x.Key.Id && z.IsDirect == x.Key.IsDirect && z.UserId == user.Id),
          dicTotal = ac.Cards.Count(z => z.Dictionary.Id == x.Key.Id),
          dicId = x.Key.Id,
          dicName = x.FirstOrDefault().Card.Dictionary.Name,
          isDirect = x.Key.IsDirect,
          updatedCnt = x.Select(y => y.UpdatedCnt).Sum(),
          studied = x.Count(),
          avgStrength = x.Select(y => y.Strength).Average(),

          weak = x.Where(y => y.Strength < S035).Count(),
          mid = x.Where(y => y.Strength >= S035 && y.Strength < S050).Count(),
          good = x.Where(y => y.Strength >= S050 && y.Strength < S070).Count(),
          strong = x.Where(y => y.Strength >= S070).Count(),

          weakReady = x.Where(y => y.Strength < S035 && DbFunctions.DiffSeconds(y.Updated, Now) > MINT + MAXT * Math.Pow(y.Strength.Value + DELTA, EXP)).Count(),


          old = x.Where(y =>
              (y.Strength >= S035 && y.UpdatedCnt >= OLDUPDATEDCNT
              && DbFunctions.DiffSeconds(y.Updated.Value, Now) > OLDINTERVAL
              && (y.Strength < 0.8 || DbFunctions.DiffSeconds(y.Updated, Now) > OLDINTERVAL2)
              && DbFunctions.DiffSeconds(y.Updated, Now) > DbFunctions.DiffSeconds(y.FirstTime, y.Updated))
            ||
              (y.Strength >= S035 && (y.UpdatedCnt < OLDUPDATEDCNT || y.Strength < S050)
              && DbFunctions.DiffSeconds(y.Updated, Now) > MK1 + MK2 * Math.Pow(y.Strength.Value, MK3))
            ).Count(),

          oldGood = x.Where(y => y.Strength >= S035
            && y.UpdatedCnt >= OLDUPDATEDCNT
            && DbFunctions.DiffSeconds(y.Updated.Value, Now) > OLDINTERVAL
            && (y.Strength < 0.8 || DbFunctions.DiffSeconds(y.Updated, Now) > OLDINTERVAL2)
            && DbFunctions.DiffSeconds(y.Updated, Now) > DbFunctions.DiffSeconds(y.FirstTime, y.Updated)).Count(),

          oldMid = x.Where(y => y.Strength >= S035
            && (y.UpdatedCnt < OLDUPDATEDCNT || y.Strength < S050)
            && DbFunctions.DiffSeconds(y.Updated, Now) > MK1 + MK2 * Math.Pow(y.Strength.Value, MK3)).Count(),
        }).ToList();
      sw.Stop();//0.16-0.3

      var SumStat = stat.Select(x => new DicStat
      {
        DicTotal = x.dicTotal,
        Skipped = x.skipped,
        Studying = x.dicTotal - x.skipped,

        AvgStrength = x.avgStrength.Value,
        Studied = x.studied,
        UserId = user.Id,
        DicId = x.dicId,
        DicName = x.dicName,
        Good = x.good,
        IsDirect = x.isDirect,
        Mid = x.mid,
        Old = x.old,
        OldMid = x.oldMid,
        OldGood = x.old - x.oldMid,
        Strong = x.strong,
        Weak = x.weak,
        Updations = x.updatedCnt,
        WeakReady = x.weakReady//,
        //IsCurrent = x.dicId == curDicId && x.isDirect == curIsDirect
      }.CountPercentage()).OrderByDescending(x => x.Updations).ToList();

      List<Mode> MixedModeList = user.GetMixedModeList(); 

      List<Mode> CurrentModes;
      if (user.MixedModeActive) CurrentModes = MixedModeList;
      else CurrentModes = new List<Mode> {
        new Mode (user.CurrentDictionary.Id, user.CurrentIsDirect)
      };

      foreach (var cm in CurrentModes)
      {
        var s = SumStat.FirstOrDefault(x => x.DicId == cm.DicId && x.IsDirect == cm.IsDirect);
        if (s != null)
        {
          s.IsCurrent = true;
        }
        else
        {
          var _dic = ac.Dictionaries.Include(x => x.Cards).Select(x => new { id = x.Id, name = x.Name, dicTotal = x.Cards.Count() }).FirstOrDefault(x => x.id == cm.DicId);

          if (_dic != null)
          {
            SumStat.Add(new DicStat()
            {
              IsDirect = cm.IsDirect,
              UserId = user.Id,
              DicId = cm.DicId,
              DicName = _dic.name,
              DicTotal = _dic.dicTotal,
              IsCurrent = true,
              Studying = _dic.dicTotal - ac.Skips.Include(x => x.Card).Count(x => x.UserId == user.Id && x.Card.DictionaryId == cm.DicId && x.IsDirect == cm.IsDirect)
            });
          }
        }        
      }

      foreach (var s in SumStat)
      {
        if (/*user.MixedModeActive && */MixedModeList.Any(x => x.DicId == s.DicId
          && x.IsDirect == s.IsDirect)) s.IsInMix = true;
      }
      return SumStat;
    }
















    public CardsToShow GetCardsAnonym(int dicId, bool isDirect, ref List<Result> results)
    {
      if (results == null) results = new List<Result>();
      int TargetWordsCnt = 5;
      Random random = new Random();
      DateTime Now = DateTime.Now;
      List<CardToShow> cardsToShow = new List<CardToShow>();

      int Weak_DB = results.Count(x => x.Card.DictionaryId == dicId && x.IsDirect == isDirect && x.Strength != null && x.Strength < 0.35);

      int WeakReady_DB = results.Count(x => x.Card.DictionaryId == dicId && x.IsDirect == isDirect && x.Strength != null && x.Strength < 0.35 && (Now - x.Updated.Value).TotalSeconds > MINT + MAXT * Math.Pow(x.Strength.Value + DELTA, EXP));
      
      int NewCnt = 0;
      if (Weak_DB <= 45)
      {
        if      (WeakReady_DB == 00) NewCnt = 5;
        else if (WeakReady_DB <= 02) NewCnt = 3 + random.Next(2);
        else if (WeakReady_DB <= 04) NewCnt = 3; // 3n 1w
        else if (WeakReady_DB <= 06) NewCnt = 2 + random.Next(2);
        else if (WeakReady_DB <= 08) NewCnt = 2; // 2n 2w
        else if (WeakReady_DB <= 10) NewCnt = 1 + random.Next(2);
        else if (WeakReady_DB <= 15) NewCnt = 1; // 1n 3w
        else if (WeakReady_DB <= 25 && random.Next(0, 3) == 0) NewCnt = 1;
      }

      if (NewCnt > 0)
      {
        var res = results;
        var cards = ac.Cards.Include(x=>x.Dictionary).Where(x => x.DictionaryId == dicId).ToList();
        cards.RemoveAll(x => res.Any(y => y.CardId == x.Id));

        cards = cards.OrderBy(x => Guid.NewGuid()).Take(NewCnt).ToList();

        foreach(var c in cards)
        {
          results.Add(new Result() { Card = c, CardId = c.Id, IsDirect = isDirect, FirstTime = Now, Showed = Now });
          cardsToShow.Add(new CardToShow
          {
            CardId = c.Id,
            IsDirect = isDirect,
            Word = c.Word,
            Translation = c.Translation,
            Transcription = c.Transcription,
            Example = c.Example
          });
        }
      }

      int WeakCnt = TargetWordsCnt - cardsToShow.Count;
      if (WeakCnt > 0)
      {
        var weakReady = results.Where(x => x.Card.DictionaryId == dicId 
        && x.IsDirect == isDirect && (
            (x.Strength != null && x.Strength < 0.35 && (Now - x.Updated.Value).TotalSeconds > MINT + MAXT * Math.Pow(x.Strength.Value + DELTA, EXP))
            ||
            (x.Strength == null && (Now - x.Showed).TotalSeconds > MINTSHOWED)
            ))
         .OrderByDescending(x => x.Strength == null)
         .ThenBy(x => x.Strength).Take(WeakCnt);
      
        foreach(var r in weakReady)
        {
          var cardInfo = r.UpdatedCnt + "-";
          if (r.Strength != null) cardInfo += r.Strength;
          cardInfo += " weak";

          var c = r.Card;
          cardsToShow.Add(new CardToShow
          {
            CardId = c.Id,
            IsDirect = isDirect,
            Word = c.Word,
            Translation = c.Translation,
            Transcription = c.Transcription,
            Example = c.Example
          });
        }
      }

      int AnyCnt = TargetWordsCnt - cardsToShow.Count;
      if (AnyCnt > 0)
      {
        var weakResults = results.Where(x => x.Card.DictionaryId == dicId && x.IsDirect == isDirect)
        .OrderByDescending(
           x => x.Strength != null
        && x.Strength < S035
        && (Now - x.Updated.Value).TotalSeconds > MINT + MAXT * Math.Pow(x.Strength.Value + DELTA, EXP))
        .ThenByDescending(x => x.Strength == null && (Now - x.Showed).TotalSeconds > MINTSHOWED)
        .ThenBy(x => x.Strength);
        
        foreach(var r in weakResults)
        {
          var cardInfo = r.UpdatedCnt + "-";
          if (r.Strength != null) cardInfo += r.Strength;
          cardInfo += " weak";

          var c = r.Card;
          cardsToShow.Add(new CardToShow
          {
            CardId = c.Id,
            IsDirect = isDirect,
            Word = c.Word,
            Translation = c.Translation,
            Transcription = c.Transcription,
            Example = c.Example
          });
        }
      }
      return new CardsToShow(cardsToShow);
    }
    
    public void SendResultAnonym(int cardId, bool isDirect, int result, ref List<Result> results)
    {
      Result res = results.FirstOrDefault(x => x.CardId == cardId && x.IsDirect == isDirect);
      if (res == null) return;

      double oldStrength = res.Strength == null ? 0.12 : res.Strength.Value;
      double newStrength = GetNewStrength(result, oldStrength, res.Updated);

      res.Strength = newStrength;
      res.UpdatedCnt++;
      res.Updated = DateTime.Now;
      return;
    }




    public IEnumerable<MiniCard[]> GetChooseCardsAnonym(int dicId, bool isDirect)
    {
      int TargetWordsCnt = 5;

      var results = ac.Cards.Where(x =>
  x.DictionaryId == dicId).OrderBy(r => Guid.NewGuid()).Take(TargetWordsCnt * 4).ToArray();

      Random ran = new Random();
      List<MiniCard[]> miniCards = new List<MiniCard[]>();
      List<MiniCard> _cards = new List<MiniCard>();
      foreach (var r in results)
      {
        MiniCard mc = new MiniCard
        {
          Id = r.Id,
          Word = r.Word,
          Translation = r.Translation,
          IsDirect = isDirect
        };
        _cards.Add(mc);

        if (_cards.Count == 4)
        {
          if (ran.Next(0, 5) <= 1)
          {
            foreach (MiniCard mc1 in _cards)
            {
              var temp = mc1.Translation;
              mc1.Translation = mc1.Word;
              mc1.Word = temp;
              mc1.IsInverse = true;
            }
          }
          miniCards.Add(_cards.ToArray());
          _cards.Clear();
        }
      }
      return miniCards;
    }

    public LastResult SendChooseResultAnonym(int cardId, bool isDirect, bool IsCorrect, int Time, int UserId)
    {
      double oldStrength = 0.12;
      double newStrength = 0;

      double min = 0;
      double max = 0.43;

      Result Result = ac.Results.Include(x => x.User).FirstOrDefault(x => x.Card.Id == cardId && x.IsDirect == isDirect && x.User.Id == UserId);////
      if (Result != null)
      {
        if (Result.Strength.HasValue)
          oldStrength = Result.Strength.Value;

        double delta = 0.06 / Math.Pow(Time / 1000.0, 1.1);
        if (delta > 0.2) delta = 0.2;
        if (IsCorrect) newStrength = oldStrength + delta;
        else newStrength = min + (max - min) * oldStrength;

        if (newStrength > 1) newStrength = 1;

        Result.Strength = newStrength;
        Result.UpdatedCnt++;
        Result.Updated = DateTime.Now;

        ac.Users.First(x => x.Id == UserId).LastActivity = DateTime.Now;
        ac.SaveChanges();
      }
      LastResult change = new LastResult
      {
        Id = Result.Id,
        OldStrength = oldStrength,
        NewStrength = newStrength
      };
      return change;
    }


  }
}