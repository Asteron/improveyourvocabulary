﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Apprendre.Models
{
  public interface IDictionaryService
  {
    IEnumerable<DictionaryBase> GetAllDictionariesVM();
    IEnumerable<Dictionary> GetAllDictionaries();
    IEnumerable<Dictionary> GetDictionaries(int[] ids);
    DictionaryVM GetDictionary(DictionaryRV routeValues, int pageSize);
    int Add(DictionaryAddVM dictionary, int authorId, out int added);
    bool Delete(int id);
    bool Edit(DictionaryEditVM dictionary);
    bool Exists(int id);
    bool Exists(IEnumerable<int> ids);
    string[] GetAllDicLanguages();
  }

  public class DictionaryService : IDictionaryService
  {
    private AppContext ac = new AppContext();

    public IEnumerable<DictionaryBase> GetAllDictionariesVM()
    {
      return ac.Dictionaries.Include(x => x.Cards).Select(x => new { Id = x.Id, Name = x.Name, Cnt = x.Cards.Count() }).ToArray().Select(x => new DictionaryBase { Id = x.Id, Name = x.Name, Total = x.Cnt }).OrderBy(x => x.Name).ToArray();
    }

    public IEnumerable<Dictionary> GetAllDictionaries()
    {
      return ac.Dictionaries;
    }

    public IEnumerable<Dictionary> GetDictionaries(int[] ids)
    {
      return ac.Dictionaries.Where(x => ids.Contains(x.Id));
    }

    public DictionaryVM GetDictionary(DictionaryRV rv, int pageSize)
    {
      var dic = ac.Dictionaries.Where(x => x.Id == rv.Id).FirstOrDefault();
      if (dic == null) return null;

      var cardsQuery = ac.Cards.Where(x => x.DictionaryId == rv.Id).OrderBy(x => x.Id);
      if (rv.Desc == true)
        switch (rv.Order)
        {
          case null:
          case 1:
            cardsQuery = cardsQuery.OrderByDescending(x => x.Id); break;
          case 2:
            cardsQuery = cardsQuery.OrderByDescending(x => x.Word); break;
          case 3:
            cardsQuery = cardsQuery.OrderByDescending(x => x.Translation); break;
        }
      else
        switch (rv.Order)
        {
          case null:
          case 1:
            cardsQuery = cardsQuery.OrderBy(x => x.Id); break;
          case 2:
            cardsQuery = cardsQuery.OrderBy(x => x.Word); break;
          case 3:
            cardsQuery = cardsQuery.OrderBy(x => x.Translation); break;
        }

      int pageNumber = rv.Page ?? 1;
      int skip = (pageNumber - 1) * pageSize;

      int totalCards = ac.Cards.Count(x => x.DictionaryId == rv.Id);

      if (pageNumber == 0)
      {
        skip = 0;
        pageSize = int.MaxValue;
      }

      dic.Cards = cardsQuery.Skip(skip).Take(pageSize).ToList();

      DictionaryVM dicVM = new DictionaryVM() {
        Dictionary = dic,
        TotalCards = totalCards,
        RouteValues = rv,
        PageInfo = new PageInfo(pageNumber, pageSize, totalCards),
        Languages = GetAllDicLanguages()
      };
      return dicVM;
    }

    public int Add(DictionaryAddVM dictionary, int authorId, out int added)
    {
      added = 0;
      Dictionary dic = ac.Dictionaries.FirstOrDefault(x => x.Name == dictionary.Name);
      if (dic != null) return -1;

      AppUser author = ac.Users.FirstOrDefault(x => x.Id == authorId);
      if (author == null) return -1;

      dic = new Dictionary
      {
        Name = dictionary.Name,
        WordLanguage = dictionary.WordLanguage,
        TranslationLanguage = dictionary.TranslationLanguage,
        ChooseExerciseAvailable = dictionary.ChooseExerciseAvailable,
        Author = author, // doesn't work with AuthorId only
        IsPublic = true,
      };

      ac.Dictionaries.Add(dic);
      ac.SaveChanges();

      added = new CardService().AddCards(dic.Id, dictionary.CardsContent);
      return dic.Id;
    }

    public bool Delete(int id)
    {
      Dictionary dic0 = ac.Dictionaries.FirstOrDefault(x => x.Id != id);
      Dictionary dic = ac.Dictionaries.FirstOrDefault(x => x.Id == id);
      if (dic0 == null || dic == null) return false;

      // It is necessary to remove dependencies because not all the constraints in the database have 'ON DELETE CASCADE'

      var results = ac.Results.Include(x => x.Card).Include(x => x.Card.Dictionary).Where(x => x.Card.Dictionary.Id == id);
      ac.Results.RemoveRange(results);

      var marks = ac.Marks.Include(x => x.Card).Where(x => x.Card.Dictionary.Id == id);
      ac.Marks.RemoveRange(marks);
      ac.SaveChanges();

      var cards = ac.Cards.Include(x => x.Dictionary).Where(x => x.Dictionary.Id == id);
      ac.Cards.RemoveRange(cards);
      ac.SaveChanges();
      
      ac.Users.Include(x => x.CurrentDictionary).Where(x => x.CurrentDictionary.Id == id).ToList().ForEach(x => x.CurrentDictionary = dic0);
      ac.SaveChanges();

      ac.Dictionaries.Remove(dic);
      ac.SaveChanges();
      return true;
    }

    public bool Edit(DictionaryEditVM dictionary)
    {
      Dictionary dic = ac.Dictionaries.FirstOrDefault(x => x.Id == dictionary.Id);
      if (dic == null) return false;
      
      dic.Name = dictionary.Name;
      dic.WordLanguage = dictionary.WordLanguage;
      dic.TranslationLanguage = dictionary.TranslationLanguage;
      dic.ChooseExerciseAvailable = dictionary.ChooseExerciseAvailable;

      ac.SaveChanges();
      return true;
    }

    public bool Exists(int Id)
    {
      return ac.Dictionaries.Any(x => x.Id == Id);
    }

    public bool Exists(IEnumerable<int> ids)
    {
      foreach(var id in ids)
      {
        if (ac.Dictionaries.Any(x => x.Id == id) == false) return false;
      }
      return true;
    }

    public string[] GetAllDicLanguages()
    {
      var languages = ac.Dictionaries.Select(x => x.WordLanguage).Concat(ac.Dictionaries.Select(x => x.TranslationLanguage)).Where(x => x != null && x.Length == 2).GroupBy(x => x).OrderByDescending(x => x.Count()).Select(x => x.Key);
      return languages.ToArray();
    }
  }
}