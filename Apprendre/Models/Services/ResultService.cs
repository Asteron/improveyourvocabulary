﻿using System.Linq;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Apprendre.Models
{
  public interface IResultService
  {
    Result GetResult(int Id);
    ResultsVM GetResults(ResultsRV routeValues, int pageSize);
    bool Update(ResultEditVM Result);
    bool Delete(int Id);
    bool DeleteAll(int userId, int dicId, bool isDirect);
    int[] ResetDateTime();
  }

  public class ResultService : IResultService
  {
    private AppContext ac = new AppContext();

    public Result GetResult(int Id)
    {
      return ac.Results.Include(x => x.Card).Include(x => x.Card.Dictionary).Include(x=>x.User).AsNoTracking().FirstOrDefault(x => x.Id == Id);
    }

    public ResultsVM GetResults(ResultsRV rv, int pageSize)
    {
      AppUser user = ac.Users.FirstOrDefault(x => x.Id == rv.UserId);
      Dictionary dic = ac.Dictionaries.FirstOrDefault(x => x.Id == rv.DicId);      

      var results = ac.Results.Include(x => x.User).Include(x => x.Card).Include(x => x.Card.Dictionary).AsNoTracking();

      if (rv.OnlyNulls == true)
      {
        results = results.Where(x => !x.Strength.HasValue);
      }
      else
      {
        results = results.Where(x => x.Strength.HasValue);
      }
      if (user != null)
      {
        rv.UserId = user.Id;
        results = results.Where(x => x.User.Id == rv.UserId);
      }
      if (dic != null)
      {
        rv.DicId = dic.Id;
        results = results.Where(x => x.Card.Dictionary.Id == rv.DicId);
      }
      if (rv.IsDirect != null)
      {
        results = results.Where(x => x.IsDirect == rv.IsDirect);
        rv.IsDirect = rv.IsDirect.Value;
      }
      if (rv.Desc == true)
      {
        if      (rv.Order == 1) results = results.OrderByDescending(x => x.Id);
        else if (rv.Order == 2) results = results.OrderByDescending(x => x.Card.Word);
        else if (rv.Order == 3) results = results.OrderByDescending(x => x.Card.Translation);
        else if (rv.Order == null 
             ||  rv.Order == 4) results = results.OrderByDescending(x => x.Strength);
        else if (rv.Order == 5) results = results.OrderByDescending(x => x.UpdatedCnt);
        else if (rv.Order == 6) results = results.OrderByDescending(x => x.FirstTime);
        else if (rv.Order == 7) results = results.OrderByDescending(x => x.Showed);
        else if (rv.Order == 8) results = results.OrderByDescending(x => x.Updated);
        else if (rv.Order == 9) results = results.OrderByDescending(x => x.IsDirect);
        else if (rv.Order == 10) results = results.OrderByDescending(x => x.Card.Dictionary.Name);
        else if (rv.Order == 11) results = results.OrderByDescending(x => x.User.UserName);
      }
      else
      {
        if      (rv.Order == 1) results = results.OrderBy(x => x.Id);
        else if (rv.Order == 2) results = results.OrderBy(x => x.Card.Word);
        else if (rv.Order == 3) results = results.OrderBy(x => x.Card.Translation);
        else if (rv.Order == null
             ||  rv.Order == 4) results = results.OrderBy(x => x.Strength);
        else if (rv.Order == 5) results = results.OrderBy(x => x.UpdatedCnt);
        else if (rv.Order == 6) results = results.OrderBy(x => x.FirstTime);
        else if (rv.Order == 7) results = results.OrderBy(x => x.Showed);
        else if (rv.Order == 8) results = results.OrderBy(x => x.Updated);
        else if (rv.Order == 9) results = results.OrderBy(x => x.IsDirect);
        else if (rv.Order == 10) results = results.OrderBy(x => x.Card.Dictionary.Name);
        else if (rv.Order == 11) results = results.OrderBy(x => x.User.UserName);
      }

      int pageNumber = rv.Page ?? 1;
      int skip = (pageNumber - 1) * pageSize;
      int totalResults = results.Count();

      if (pageNumber == 0)
      {
        skip = 0;
        pageSize = int.MaxValue;
      }


      results = results.Skip(skip).Take(pageSize);

      UserModeVM modeVM = null;
      if (rv.UserId != null && rv.DicId != null && rv.IsDirect != null 
        && user != null && dic != null)
      {
        modeVM = new UserModeVM
        {
          UserId = rv.UserId.Value,
          UserName = user.UserName,
          DicId = rv.DicId.Value,
          DicName = dic.Name,
          IsDirect = rv.IsDirect.Value
        };
      }

      ResultsVM resVM = new ResultsVM()
      {
        RouteValues = rv,
        Results = results.ToArray(),
        Mode = modeVM,
        PageInfo = new PageInfo(pageNumber, pageSize, totalResults)
      };
      return resVM;
    }

    public bool Update(ResultEditVM result)
    {
      Result dbResult = ac.Results.Include(x => x.Card).Include(x => x.Card.Dictionary).FirstOrDefault(x => x.Id == result.Id);

      if (dbResult == null || !result.Updated.HasValue
        || !result.Strength.HasValue) return false;

      dbResult.FirstTime = result.FirstTime;
      dbResult.Showed = result.Showed;
      dbResult.Updated = result.Updated;
      dbResult.UpdatedCnt = result.UpdatedCnt;
      dbResult.Strength = result.Strength;
      
      ac.SaveChanges();
      return true;
    }

    public bool Delete(int id)
    {
      var result = ac.Results.FirstOrDefault(x => x.Id == id);
      if (result == null) return false;

      ac.Results.Remove(result);
      ac.SaveChanges();
      return true;
    }

    public bool DeleteAll(int UserId, int DicId, bool IsDirect)
    {
      var results = ac.Results.Include(x => x.User).Include(x => x.Card).Include(x => x.Card.Dictionary).Where(x => x.User.Id == UserId && x.Card.Dictionary.Id == DicId && x.IsDirect == IsDirect);
      ac.Results.RemoveRange(results);
      ac.SaveChanges();
      return true;
    }



    public int[] ResetDateTime()
    {
      int[] output = new int[2];

      var nullResults = ac.Results.Where(x => !x.Strength.HasValue);
      ac.Results.RemoveRange(nullResults);
      output[0] = ac.SaveChanges() / 3;

      DateTime Now = DateTime.Now;
      var results = ac.Results.Where(x => x.Showed > Now || (x.Updated.HasValue && x.Updated > Now));
      foreach (var r in results)
      {
        if (r.Showed > Now) r.Showed = Now;
        if (r.Updated.HasValue && r.Updated > Now) r.Updated = Now;
      }
      output[1] = ac.SaveChanges();

      return output;
    }



  }
}