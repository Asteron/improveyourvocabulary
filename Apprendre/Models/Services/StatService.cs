﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Text;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;

namespace Apprendre.Models
{
  public interface IStatService
  {
    UserDictionaryVM GetDicResults(UserDictionaryRV rv, int pageSize);
    bool SkipCard(int userId, int cardId, bool isDirect, bool skip);

    double[][] GetStrengthRanksData(int userId, int dicId, bool isDirect);
    string GetDaysData(int userId, int dicId, bool isDirect);

    IEnumerable<Result> GetWeakestResults(int userId, int dicId, bool isDirect, int count);

    IEnumerable<Result> GetLastResults(int userId, int count);
    IEnumerable<Result> GetMaxCntResults(int userId, int count);
  }

  public class StatService : IStatService
  {
    AppContext ac = new AppContext();

    public UserDictionaryVM GetDicResults(UserDictionaryRV rv, int pageSize)
    {
      //Stopwatch sw = new Stopwatch(); sw.Start();
      var cards = ac.Cards.Where(x => x.DictionaryId == rv.DicId).Include(x => x.Results).Include(x => x.Skips).Select(x => new
      {
        card = x,
        result = x.Results.FirstOrDefault(y => y.UserId == rv.Id && y.IsDirect == rv.IsDirect),
        skipped = x.Skips.Any(y => y.UserId == rv.Id && y.IsDirect == rv.IsDirect)
      });

      if (rv.Desc == true)     
        switch (rv.Order)
        {
          case null:
          case 1:
            cards = cards.OrderByDescending(x => x.card.Id); break;
          case 2:
            cards = cards.OrderByDescending(x => x.card.Word); break;
          case 3:
            cards = cards.OrderByDescending(x => x.card.Translation); break;
          case 4:
            cards = cards.OrderBy(x => x.skipped)
              .ThenByDescending(x => x.result != null && x.result.Strength != null)
              .ThenByDescending(x => x.result.Strength); break;
        }
      else
        switch (rv.Order)
        {
          case null:
          case 1:
            cards = cards.OrderBy(x => x.card.Id); break;
          case 2:
            cards = cards.OrderBy(x => x.card.Word); break;
          case 3:
            cards = cards.OrderBy(x => x.card.Translation); break;
          case 4:
            cards = cards.OrderByDescending(x => x.skipped)
              .ThenBy(x => x.result != null && x.result.Strength != null)
              .ThenBy(x => x.result.Strength); break;
        }

      int pageNumber = rv.Page ?? 1;
      int skip = (pageNumber - 1) * pageSize;
      int totalResults = cards.Count();

      if (pageNumber == 0)
      {
        skip = 0;
        pageSize = int.MaxValue;
      }

      cards = cards.Skip(skip).Take(pageSize);


      List<DicResult> list = new List<DicResult>();
      foreach(var card in cards)
      {
        DicResult dr = new DicResult() { Card = card.card, IsSkipped = card.skipped };
        if (card.result != null&&card.result.Strength != null)
        {
          dr.ResultId = card.result.Id;
          dr.Strength = card.result.Strength.Value;
          dr.UpdatedCnt = card.result.UpdatedCnt;
        }
        list.Add(dr);
      }

      UserDictionaryVM vm = new UserDictionaryVM {
        DicId = rv.DicId,
        UserId = rv.Id,
        IsDirect = rv.IsDirect,
        RouteValues = rv,
        DicResults = list,
        PageInfo = new PageInfo(pageNumber, pageSize, totalResults)
      };

      vm.UserName = ac.Users.FirstOrDefault(x => x.Id == rv.Id).UserName;
      vm.DicName = ac.Dictionaries.FirstOrDefault(x => x.Id == rv.DicId).Name;      

      //sw.Stop();//0.3-0.05 в зав. от словаря
      return vm;
    }

    public bool SkipCard(int userId, int cardId, bool isDirect, bool skip)
    {
      var curSkip = ac.Skips.FirstOrDefault(x => x.UserId == userId && x.CardId == cardId && x.IsDirect == isDirect);

      if (!skip && curSkip != null)
      {
        ac.Skips.Remove(curSkip);
        ac.SaveChanges();
        return true;
      }
      else if (skip && curSkip == null)
      {
        Card card = ac.Cards.FirstOrDefault(x => x.Id == cardId);
        AppUser user = ac.Users.FirstOrDefault(x => x.Id == userId);
        if (card == null || user == null) return false;

        Skip s = new Skip { User = user, Card = card, IsDirect = isDirect };
        ac.Skips.Add(s);

        Result r = ac.Results.FirstOrDefault(x => x.UserId == userId && x.CardId == cardId && x.IsDirect == isDirect);
        if (r != null) ac.Results.Remove(r);

        ac.SaveChanges();
        return true;
      }
      return false;
    }

    public double[][] GetStrengthRanksData(int userId, int dicId, bool isDirect)
    {
      int cnt = 50;
      var extended = ac.Results.Include(x => x.User).Include(x => x.Card).Include(x => x.Card.Dictionary).Where(x => x.User.Id == userId && x.Card.Dictionary.Id == dicId && x.IsDirect == isDirect && x.Strength.HasValue).GroupBy(x => Math.Floor(x.Strength.Value * cnt) / cnt).Select(x => new { str = x.Key, cnt = x.Count() }).OrderBy(x => x.str).ToArray();

      List<double[]> list = new List<double[]>(cnt + 1);

      foreach (var r in Enumerable.Range(0, cnt + 1))
      {
        double strength = (double)r / cnt;
        var rr = extended.FirstOrDefault(x => x.str == strength);
        double[] arr = { strength, 0 };
        if (rr != null) arr[1] = rr.cnt;
        list.Add(arr);
      }
      return list.ToArray();
    }

    public string GetDaysData(int userId, int dicId, bool isDirect)
    {
      var datesData = ac.Results.Include(x => x.User).Include(x => x.Card).Include(x => x.Card.Dictionary).Where(x => x.User.Id == userId && x.Card.Dictionary.Id == dicId && x.IsDirect == isDirect && x.Strength.HasValue).GroupBy(x => EntityFunctions.TruncateTime(x.FirstTime)).Select(x => new { date = x.Key, cnt = x.Count() }).OrderBy(x => x.date).ToArray();
      //,[new Date(2018, 0, 02),55],

      bool first = true;
      StringBuilder sb = new StringBuilder(256);
      sb.Append('[');
      foreach (var item in datesData)
      {
        if (first) first = false;
        else sb.Append(',');
        sb.Append("[new Date(");
        sb.Append(item.date.Value.Year);
        sb.Append(',');
        sb.Append(item.date.Value.Month - 1);
        sb.Append(',');
        sb.Append(item.date.Value.Day);
        sb.Append("),");
        sb.Append(item.cnt);
        sb.Append(']');
      }
      sb.Append(']');
      return sb.ToString();
    }

    public IEnumerable<Result> GetWeakestResults(int userId, int dicId, bool isDirect, int count)
    {
      var results = ac.Results.Include(x => x.User).Include(x => x.Card).Include(x => x.Card.Dictionary).Where(x => x.User.Id == userId && x.Card.Dictionary.Id == dicId && x.IsDirect == isDirect && x.Strength.HasValue).OrderBy(x => x.Strength).Take(count).ToArray();
      return results;
    }





    public IEnumerable<Result> GetLastResults(int userId, int count)
    {
      return ac.Results.Include(x => x.Card).Include(x => x.Card.Dictionary).Where(x => x.UserId == userId && x.Strength != null).OrderByDescending(x => x.Updated).Take(count).AsNoTracking().ToArray();
    }

    public IEnumerable<Result> GetMaxCntResults(int userId, int count)
    {
      return ac.Results.Include(x => x.Card).Include(x => x.Card.Dictionary).Where(x => x.UserId == userId && x.Strength != null).OrderByDescending(x => x.UpdatedCnt).Take(count).AsNoTracking().ToArray();
    }



  }
}