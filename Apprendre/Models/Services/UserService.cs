﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using Newtonsoft.Json;

namespace Apprendre.Models
{
  public interface IUserService
  {
    bool ChangeMode(int userId, int dicId, bool isDirect, int? limit, bool? randomOrder);
    bool ChangeMixedMode(int userId, ModeSettingEditVM[] modes);
    bool SwitchToMixedMode(int userId);
    bool ChangeSettings(int userId, string email, double timezone, string language);

    ModeSetting GetModeSettings(int userId, int dicId, bool isDirect);
    IEnumerable<ModeSetting> GetMixedModeSettings(int userId);

    IEnumerable<AppUser> GetAllUsers();

    bool SaveUnsavedResults(List<Result> results, int userId);
  }

  public class UserService : IUserService
  {
    private AppContext ac = new AppContext();
       
    public bool ChangeMode(int userId, int dicId, bool isDirect, int? limit, bool? randomOrder)
    {
      var user = ac.Users.FirstOrDefault(x => x.Id == userId);
      if (user == null) return false;

      user.CurrentDictionaryId = dicId;
      user.CurrentIsDirect = isDirect;
      user.MixedModeActive = false;

      if (randomOrder.HasValue)
      {
        int _limit = 0;
        if (limit.HasValue && limit.Value > 0) _limit = limit.Value;
        bool _randomOrder = randomOrder.Value;

        ModeSetting ms = ac.ModeSettings.FirstOrDefault(x => x.DictionaryId == dicId && x.AppUserId == userId);

        if (ms != null)
        {
          if (_limit == 0 && _randomOrder == false)
          {
            ac.ModeSettings.Remove(ms);
          }
          else
          {
            ms.StudyLimit = _limit;
            ms.RandomOrder = _randomOrder;
          }
        }
        else if (_limit > 0 || _randomOrder)
        {
          Dictionary dic = ac.Dictionaries.FirstOrDefault(x => x.Id == dicId);
          ms = new ModeSetting
          {
            AppUser = user,
            Dictionary = dic,
            IsDirect = isDirect,
            StudyLimit = _limit,
            RandomOrder = _randomOrder
          };
          ac.ModeSettings.Add(ms);
        }
      }
      ac.SaveChanges();

      var user3 = ac.Users.Include(x => x.CurrentDictionary).FirstOrDefault(x => x.Id == userId);

      var ir = ac.Users.Include(x => x.CurrentDictionary).FirstOrDefault(x => x.Id == 2);
      return true;
    }

    public bool ChangeMixedMode(int userId, ModeSettingEditVM[] modeSettings)
    {
      var user = ac.Users.FirstOrDefault(x => x.Id == userId);
      if (user == null) return false;

      user.MixedModeActive = true;
      var modes = modeSettings.Select(x => new int[] { x.DicId, x.IsDirect ? 1 : 0 });
      user.MixedModeSer = JsonConvert.SerializeObject(modes);
      ac.SaveChanges();

      ChangeModeSettings(user, modeSettings);
      return true;
    }

    private void ChangeModeSettings(AppUser user, ModeSettingEditVM[] modeSettings)
    {
      var dbSettings = ac.ModeSettings.Where(x => x.AppUserId == user.Id);
      var ids = modeSettings.Select(x => x.DicId);
      var dictionaries = ac.Dictionaries.Where(x => ids.Contains(x.Id));

      foreach (var ms in modeSettings)
      {
        var dbs = dbSettings.FirstOrDefault(x => x.DictionaryId == ms.DicId && x.IsDirect == ms.IsDirect);
        if (ms.StudyLimit >= 0 || ms.RandomOrder == true)
        {
          if (dbs == null)
          {
            var dictionary = dictionaries.FirstOrDefault(x => x.Id == ms.DicId);
            if (dictionary == null) continue;

            ModeSetting modeSetting = new ModeSetting
            {
              AppUser = user,
              Dictionary = dictionary,
              IsDirect = ms.IsDirect
            };
            if (ms.StudyLimit > 0) modeSetting.StudyLimit = ms.StudyLimit.Value;
            if (ms.RandomOrder == true) modeSetting.RandomOrder = true;
            ac.ModeSettings.Add(modeSetting);
          }
          else if (dbs.StudyLimit != ms.StudyLimit || dbs.RandomOrder != ms.RandomOrder)
          {
            if (ms.StudyLimit.HasValue) dbs.StudyLimit = ms.StudyLimit.Value;
            if (ms.RandomOrder.HasValue) dbs.RandomOrder = ms.RandomOrder.Value;
          }
        }
        else if (dbs != null)
        {
          ac.ModeSettings.Remove(dbs);            
        }          
      }
      ac.SaveChanges();
    }
    
    public bool SwitchToMixedMode(int userId)
    {
      var user = ac.Users.FirstOrDefault(x => x.Id == userId);
      if (user == null || user.GetMixedModeList().Count < 1) return false;

      user.MixedModeActive = true;      
      ac.SaveChanges();
      return true;
    }

    public bool ChangeSettings(int userId, string email, double timezone, string language)
    {
      var user = ac.Users.FirstOrDefault(x => x.Id == userId);
      if (user == null) return false;

      if (email != null)
      {
        user.Email = email;
        user.EmailConfirmed = false;
      }
      if (user.Language != null)
        user.Language = language;

      user.Timezone = timezone;
      ac.SaveChanges();
      return true;
    }


    public ModeSetting GetModeSettings(int userId, int dicId, bool isDirect)
    {
      var user = ac.Users.FirstOrDefault(x => x.Id == userId);
      if (user == null) return null;

      var ms = ac.ModeSettings.FirstOrDefault(x => x.AppUserId == userId && x.DictionaryId == dicId && x.IsDirect == isDirect);
      if (ms == null) ms = new ModeSetting();
      return ms;
    }

    public IEnumerable<ModeSetting> GetMixedModeSettings(int userId)
    {
      var user = ac.Users.FirstOrDefault(x => x.Id == userId);
      if (user == null) return null;

      List<Mode> modes = user.GetMixedModeList();

      int[] dicIds = modes.Select(x => x.DicId).ToArray();
      var dictionaries = ac.Dictionaries.Where(x => dicIds.Contains(x.Id));

      List<ModeSetting> allDbUserMS = ac.ModeSettings.Where(x => x.AppUserId == user.Id).ToList();
      List<ModeSetting> output = new List<ModeSetting>();

      foreach (Mode m in modes)
      {
        var dic = dictionaries.FirstOrDefault(x => x.Id == m.DicId);
        if (dic == null) continue;

        ModeSetting ms = new ModeSetting
        {
          Dictionary = dic,
          DictionaryId = dic.Id,
          IsDirect = m.IsDirect
        };

        var dbModeSettings = allDbUserMS.FirstOrDefault(x => x.DictionaryId == m.DicId && x.IsDirect == m.IsDirect);
        if (dbModeSettings != null)
        {
          ms.StudyLimit = dbModeSettings.StudyLimit;
          ms.RandomOrder = dbModeSettings.RandomOrder;
        }
        output.Add(ms);
      }
      return output;
    }


    public IEnumerable<AppUser> GetAllUsers()
    {
      return ac.Users.Include(x => x.CurrentDictionary).Include(x => x.Roles).AsNoTracking().ToArray();
    }


    public bool SaveUnsavedResults(List<Result> results, int userId)
    {
      var user = ac.Users.FirstOrDefault(x => x.Id == userId);
      if (user == null) return false;

      ac.Entry(user).Collection(x => x.Results).Load();

      results = results.Where(x => x.Strength.HasValue).ToList();
      int[] cardsId = results.Select(x => x.CardId).ToArray();
      Card[] cards = ac.Cards.Where(x => cardsId.Contains(x.Id)).ToArray();

      foreach(var r in results)
      {
        Card c = cards.First(x => x.Id == r.CardId);
        r.Card = c;
        r.User = user;
        r.UserId = user.Id;
      }
      user.Results.AddRange(results);
      ac.SaveChanges();
      return true;
    }
  }
}