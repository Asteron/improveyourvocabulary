﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public static class SiteLanguages
  {
    public static readonly Language[] List = new Language[] 
    { 
      new Language { Code = "ru", SelfName = "Русский"},
      new Language { Code = "en", SelfName = "English"},
      new Language { Code = "pl", SelfName = "Polski"} 
    }; 

    public class Language
    {
      public string Code { get; set; }
      public string SelfName { get; set; }
    }
  }
}