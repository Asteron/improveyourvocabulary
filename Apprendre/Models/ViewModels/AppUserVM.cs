﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class AppUserVM
  {
    public AppUser AppUser { get; set; }
    public IEnumerable<string> Roles { get; set; }
    public IEnumerable<NamedMode> CurrentModes { get; set; }
  }
}