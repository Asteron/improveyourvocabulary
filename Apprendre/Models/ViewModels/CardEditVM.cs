﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class CardEditVM : ICard
  {
    public int Id { get; set; }
        
    [Required]
    [StringLength(100)]
    public string Word { get; set; }

    [Required]
    [StringLength(200)]    
    public string Translation { get; set; }

    [StringLength(100)]
    public string Transcription { get; set; }

    [StringLength(500)]
    public string Example { get; set; }

    public bool Marked { get; set; }
  }
}