﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Apprendre.Models
{
  public class CardToShow : ICard
  {
    public int CardId { get; set; }
    public string Word { get; set; }
    public string Translation { get; set; }
    public string Transcription { get; set; }
    public string Example { get; set; }

    public bool IsDirect { get; set; }
    public string CardInfo { get; set; }
  }

  public class CardsToShow
  {
    public List<CardToShow> Cards { get; set; }
    public string Info { get; private set; }

    public CardsToShow(List<CardToShow> cardsToShow)
    {
      Cards = cardsToShow;
    }

    public CardsToShow(List<SortableCard> SortableCards, bool NeedInfo)
    {
      if (NeedInfo)
      {
      Info = string.Join(" ", "ownarm".Select(x => new { Type = x, cnt = SortableCards.Count(t => t.Type == x) }).Select(x => x.cnt > 0 ? x.cnt.ToString() + x.Type : "").ToArray());

      foreach(var sc in SortableCards)
      {
        Result Result = sc.Result;
        string cardInfo = "new";
        if (Result.Updated.HasValue)
        {
          string u = "";
          var ua = DateTime.Now - Result.Updated.Value;
          if (ua.TotalDays > 1) u = Math.Round(ua.TotalDays, 1) + "d";
          else if (ua.TotalHours > 1) u = Math.Round(ua.TotalHours, 1) + "h";
          else u = Math.Round(ua.TotalMinutes) + "m";

          cardInfo = Result.Strength.Value.ToString("F2") + " - "
            + Result.UpdatedCnt + "&thinsp;tms " + u + " " + sc.Type;
        }
        sc.CardToShow.CardInfo = cardInfo;
      }

      }
      this.Cards = SortableCards.Select(x => x.CardToShow).ToList();
    }
    
  }

  public class SortableCard
  {
    public CardToShow CardToShow { get; private set; }
    public char Type { get; private set; }
    public Result Result { get; set; }

    public SortableCard(Result result, string type)
    {
      Result = result;
      Type = type[0];

      CardToShow = new CardToShow()
      {
        CardId = result.Card.Id,
        Word = result.Card.Word,
        Translation = result.Card.Translation,
        Transcription = result.Card.Transcription,
        Example = result.Card.Example,
        IsDirect = result.IsDirect
      };
    }

  
    
  }



}