﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class DicResult
  {
    public Card Card { get; set; }
    public bool IsSkipped { get; set; }

    public int ResultId { get; set; }
    public int UpdatedCnt { get; set; }
    public double Strength { get; set; }
  }
}