﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class DicStat : NamedMode
  {
    public int DicTotal { get; set; }

    public int Skipped { get; set; }
    public int Studying { get; set; }

    public int Studied { get; set; }
    public int Updations { get; set; }
    public double AvgStrength { get; set; }

    public int WeakReady { get; set; }
    public int WeakReadyP { get; set; }
    public int Weak { get; set; }
    public int WeakP { get; set; }
    public int Mid { get; set; }
    public int MidP { get; set; }
    public int Good { get; set; }
    public int GoodP { get; set; }
    public int Strong { get; set; }
    public int StrongP { get; set; }

    public int Old { get; set; }
    public int OldP { get; set; }
    public int OldMid { get; set; }
    public int OldMidP { get; set; }
    public int OldGood { get; set; }
    public int OldGoodP { get; set; }

    public int UserId { get; set; }
    //public int DicId { get; set; }
    //public string DicName { get; set; }
    //public bool IsDirect { get; set; }
    public bool DicChooseAvailable { get; set; }

    //public bool IsCurrent { get; set; } // (is bold)
    public bool IsInMix { get; set; }     // (/ mixed)

    public object LastResult { get; set; }

    public DicStat CountPercentage()
    {
      if (Studied > 0)
      {
        WeakP = (int)Math.Round(Weak * 100.0 / Studied, 0);
        MidP = (int)Math.Round(Mid * 100.0 / Studied, 0);
        GoodP = (int)Math.Round(Good * 100.0 / Studied, 0);
        StrongP = (int)Math.Round(Strong * 100.0 / Studied, 0);
        WeakReadyP = (int)Math.Round(WeakReady * 100.0 / Studied, 0);
        OldP = (int)Math.Round(Old * 100.0 / Studied, 0);
        OldMidP = (int)Math.Round(OldMid * 100.0 / Studied, 0);
        OldGoodP = (int)Math.Round(OldMid * 100.0 / Studied, 0);
      }
      return this;
    }
  }
}