﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class DictionaryAddVM : IDictionary
  {
    [Required]
    [StringLength(70, MinimumLength = 3)]
    public string Name { get; set; }

    [StringLength(2)]
    public string WordLanguage { get; set; }

    [StringLength(2)]
    public string TranslationLanguage { get; set; }

    public bool ChooseExerciseAvailable { get; set; }

    [Required]
    public string CardsContent { get; set; }
  }
}