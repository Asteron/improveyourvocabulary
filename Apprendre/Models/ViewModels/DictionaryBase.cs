﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class DictionaryBase
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public int Total { get; set; }
  }
}