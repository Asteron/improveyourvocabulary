﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class DictionaryVM
  {
    public Dictionary Dictionary { get; set; }
    public int TotalCards { get; set; }

    public DictionaryRV RouteValues { get; set; }
    public PageInfo PageInfo { get; set; }

    public string[] Languages { get; set; }
  }
}