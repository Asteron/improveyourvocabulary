﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Apprendre
{
  public class LastResult
  {
    public double OldStrength { get; set; }
    public double NewStrength { get; set; }
    public int Id { get; set; }
    public int DicId { get; set; }
  }
}
