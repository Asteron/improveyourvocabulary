﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class MiniCard
  {
    public int Id { get; set; }
    public string Word { get; set; }
    public string Translation { get; set; }
    public bool IsDirect { get; set; }
    public bool IsInverse { get; set; }
  }
}