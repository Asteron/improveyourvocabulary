﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class Mode
  {
    public int DicId { get; set; }
    public bool IsDirect { get; set; }

    public Mode()
    { }

    public Mode(int dicId, bool isDirect)
    {
      DicId = dicId;
      IsDirect = isDirect;
    }
  }
}