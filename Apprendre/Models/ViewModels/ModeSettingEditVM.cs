﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class ModeSettingEditVM : Mode
  {
    public int? StudyLimit { get; set; }
    public bool? RandomOrder { get; set; }
  }
}