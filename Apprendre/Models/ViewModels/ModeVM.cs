﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class NamedMode : Mode
  {
    public string DicName { get; set; }
    public bool IsCurrent { get; set; } // (is bold)
  }
}