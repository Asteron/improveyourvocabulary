﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class PageInfo
  {
    public int PageNumber { get; private set; }

    private readonly int TotalItems;
    private readonly int PageSize;

    public PageInfo(int PageNumber, int PageSize, int TotalItems)
    {
      this.PageNumber = PageNumber;
      this.PageSize = PageSize;
      this.TotalItems = TotalItems;
    }

    public int TotalPages
    {
      get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
    }

    public int SkippedItems
    {
      get
      {
        return PageNumber == 0 ? 0 : (PageNumber - 1) * PageSize;
      }
    }
  }
}