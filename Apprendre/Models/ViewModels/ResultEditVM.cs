﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class ResultEditVM : IResult
  {
    public int Id { get; set; }

    
    public DateTime FirstTime { get; set; }

    public DateTime Showed { get; set; }

    public DateTime? Updated { get; set; }

    public int UpdatedCnt { get; set; }

    public double? Strength { get; set; }
  }
}