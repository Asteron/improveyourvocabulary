﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class ResultsVM
  {
    public Result[] Results { get; set; }
    public UserModeVM Mode { get; set; }

    public ResultsRV RouteValues { get; set; }
    public PageInfo PageInfo { get; set; }
  }
}