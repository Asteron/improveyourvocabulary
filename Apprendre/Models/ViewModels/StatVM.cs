﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class UserStatVM : UserModeVM
  {
    public DicStat DicStat { get; set; }
    public string RanksData { get; set; }
    public string DaysData { get; set; }
    public IEnumerable<Result> WeakResults { get; set; }
  }
}