﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Apprendre.Models
{
  public class StudyVM
  {
    public bool IsAuthenticated { get; set; }
    public bool Training2Available { get; set; }
    
    public List<SelectListItem> Dictionaries { get; set; }
    public bool IsDicTrySet { get; set; }
  }
}