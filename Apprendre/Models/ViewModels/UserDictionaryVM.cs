﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  public class UserDictionaryVM : UserModeVM
  {
    public IEnumerable<DicResult> DicResults { get; set; }

    public UserDictionaryRV RouteValues { get; set; }

    public PageInfo PageInfo { get; set; }
  }
}