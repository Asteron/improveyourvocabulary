﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apprendre.Models
{
  // Used for UserNav Panel
  public class UserModeVM
  {
    public int UserId { get; set; }
    public string UserName { get; set; }

    public int DicId { get; set; }
    public string DicName { get; set; }

    public bool IsDirect { get; set; }

    public bool IsOwner { get; set; }
  }
}