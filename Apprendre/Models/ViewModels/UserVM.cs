﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Apprendre.Models
{
  public class UserVM : UserModeVM
  {
    public AppUser AppUser { get; set; }

    public IEnumerable<DicStat> SumStat { get; set; }
    public DicStat CurDicStat { get; set; }
    public ModeSetting CurrentModeSettings { get; set; }
    public IEnumerable<ModeSetting> MixedModeList { get; set; }
    public List<SelectListItem> Dictionaries { get; set; }

    public IEnumerable<Result> LastResults { get; set; }
    public IEnumerable<Result> MaxCntResults { get; set; }
  }
}