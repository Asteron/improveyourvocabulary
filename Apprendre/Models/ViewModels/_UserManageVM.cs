﻿using Apprendre.Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Apprendre.Models
{
  public class SettingsVM
  {
    public int? FontSize { get; set; }
    public double Timezone { get; set; }

    [Required(ErrorMessageResourceName = "uRequiredField",
      ErrorMessageResourceType = typeof(Resource))]
    public string Language { get; set; }

    [Display(Name = "_Email", ResourceType = typeof(Resource))]
    [EmailAddress(ErrorMessage = null,
      ErrorMessageResourceName = "uEnterValidEmail",
      ErrorMessageResourceType = typeof(Resource))]
    [RegularExpression(@"[a-z0-9._%+-]{1,50}@[a-z0-9.-]{1,50}\.[a-z]{2,5}",
      ErrorMessage = null,
      ErrorMessageResourceName = "uEnterValidEmail",
      ErrorMessageResourceType = typeof(Resource))]
    public string Email { get; set; }
  }

  public class SetNewPasswordVM
  {
    [Required(ErrorMessageResourceName = "uRequiredField",
      ErrorMessageResourceType = typeof(Resource))]
    public int UserId { get; set; }

    [Required(ErrorMessageResourceName = "uRequiredField",
      ErrorMessageResourceType = typeof(Resource))]
    public string Token { get; set; }

    [Display(Name = "uNewPassword", ResourceType = typeof(Resource))]
    [DataType(DataType.Password)]
    [Required(ErrorMessageResourceName = "uRequiredField",
      ErrorMessageResourceType = typeof(Resource))]
    [StringLength(20,
      ErrorMessageResourceName = "uPasswordMustContain",
      ErrorMessageResourceType = typeof(Resource),
      MinimumLength = 6)]
    public string NewPassword { get; set; }

    [Display(Name = "uPasswordConfirmation", ResourceType = typeof(Resource))]
    [DataType(DataType.Password)]
    [Required(ErrorMessageResourceName = "uRequiredField",
      ErrorMessageResourceType = typeof(Resource))]
    [Compare("NewPassword",
      ErrorMessageResourceName = "uPasswordsDontMatch",
      ErrorMessageResourceType = typeof(Resource))]
    public string ConfirmPassword { get; set; }
  }


  public class ChangePasswordVM
  {
    [Display(Name = "uCurrentPassword", ResourceType = typeof(Resource))]
    [DataType(DataType.Password)]
    [Required(ErrorMessageResourceName = "uRequiredField", 
      ErrorMessageResourceType = typeof(Resource))]
    public string OldPassword { get; set; }

    [Display(Name = "uNewPassword", ResourceType = typeof(Resource))]
    [DataType(DataType.Password)]
    [Required(ErrorMessageResourceName = "uRequiredField", 
      ErrorMessageResourceType = typeof(Resource))]
    [StringLength(20, ErrorMessageResourceName = "uPasswordMustContain", 
      ErrorMessageResourceType = typeof(Resource), MinimumLength = 6)]
    public string NewPassword { get; set; }

    [Display(Name = "uPasswordConfirmation", ResourceType = typeof(Resource))]
    [DataType(DataType.Password)]
    [Required(ErrorMessageResourceName = "uRequiredField", 
      ErrorMessageResourceType = typeof(Resource))]
    [Compare("NewPassword", 
      ErrorMessageResourceName = "uPasswordsDontMatch", 
      ErrorMessageResourceType = typeof(Resource))]
    public string ConfirmPassword { get; set; }
  }

  public class ResetPasswordVM
  {
    [Display(Name = "_Email", ResourceType = typeof(Resource))]
    [Required(ErrorMessageResourceName = "uRequiredField", 
      ErrorMessageResourceType = typeof(Resource))]
    public string Email { get; set; }
  }



  public class LoginVM
  {
    [Display(Name = "_UserName", ResourceType = typeof(Resource))]
    [Required(ErrorMessageResourceName = "uRequiredField",
      ErrorMessageResourceType = typeof(Resource))]
    public string UserName { get; set; }

    [Display(Name = "_Password", ResourceType = typeof(Resource))]
    [DataType(DataType.Password)]
    [Required(ErrorMessageResourceName = "uRequiredField",
      ErrorMessageResourceType = typeof(Resource))]
    public string Password { get; set; }

    [Display(Name = "uRememberMe", ResourceType = typeof(Resource))]
    public bool RememberMe { get; set; }
  }

  public class RegisterVM
  {
    [Display(Name = "_UserName", ResourceType = typeof(Resource))]
    [Required(ErrorMessageResourceName = "uRequiredField",
      ErrorMessageResourceType = typeof(Resource))]
    [RegularExpression(@"([a-zA-Z0-9_-]|[а-яА-Я0-9_-])+", 
      ErrorMessageResourceName = "uInvalidCharCombination", 
      ErrorMessageResourceType = typeof(Resource))]
    [StringLength(20, ErrorMessageResourceName = "uPasswordMustContain", 
      ErrorMessageResourceType = typeof(Resource), MinimumLength = 3)]
    public string UserName { get; set; }


    [Display(Name = "_Email", ResourceType = typeof(Resource))]
    [Required(ErrorMessageResourceName = "uRequiredField",
      ErrorMessageResourceType = typeof(Resource))]
    [EmailAddress(ErrorMessage = null,
      ErrorMessageResourceName = "uEnterValidEmail",
      ErrorMessageResourceType = typeof(Resource))]
    [RegularExpression(@"[a-z0-9._%+-]{1,50}@[a-z0-9.-]{1,50}\.[a-z]{2,5}",
      ErrorMessage = null,
      ErrorMessageResourceName = "uEnterValidEmail",
      ErrorMessageResourceType = typeof(Resource))]
    public string Email { get; set; }

    [Display(Name = "_Password", ResourceType = typeof(Resource))]
    [DataType(DataType.Password)]
    [Required(ErrorMessageResourceName = "uRequiredField", 
      ErrorMessageResourceType = typeof(Resource))]
    [StringLength(20, 
      ErrorMessageResourceName = "uPasswordMustContain", 
      ErrorMessageResourceType = typeof(Resource), MinimumLength = 6)]
    public string Password { get; set; }

    [Display(Name = "uPasswordConfirmation", ResourceType = typeof(Resource))]
    [DataType(DataType.Password)]
    [Compare("Password", 
      ErrorMessageResourceName = "uPasswordsDontMatch", 
      ErrorMessageResourceType = typeof(Resource))]
    public string ConfirmPassword { get; set; }

    public string Language { get; set; }
  }

  public class ErrorVM
  {
    public ErrorVM(string Message)
    {
      this.Message = Message;
    }
    public string Message { get; private set; }
  }
}