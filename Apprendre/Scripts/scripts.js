﻿var cards = [];
var card = {};
var started = false;
var chStarted = false;
var prevStat = {};
var initStat = undefined;
var cardsCnt = 0;
var RefreshOnModeChanged = false;
var tryingOut = false;
var waitingCorrect = false;

var chAll = [];
var curCh = [];
var error = false;

window.onload = function () {

  $(window).on('resize', function () {
    if (ShrinkHeader) ShrinkHeader();
    if (ShrinkNav !== undefined) {
      ShrinkNav();
    }
  });

  var fontSize = getCookie('fontSize');
  if (fontSize) {
    var fs = parseInt(fontSize);
    $('.scalable').css('font-size', fs + '%');
    $('#FontSize').val(fontSize);
    if (fs > 120) {
      $('.ch-translation').css('width', '95%')
    }
  }
  else $('#FontSize').val(100);

  $("#mini-stat").on('click', function () {
    if (initStat !== undefined) {
      $('#mini-stat-diff').show();
      $('#mini-stat-line3').hide();

      setTimeout(function () {
        $('#mini-stat-diff').hide();
        $('#mini-stat-line3').show();
      }, 10000);
    }
  });

  if (is_touch_device()) {
    $('#link-user > a').attr('href', '#')
    $('#link-dic > a').attr('href', '#')
  }

  $('#user-dd').css('min-width', $('#link-user').outerWidth());

  var bodyWidth = $('.body-content').width();
  var allResWidth = $('#all-results').width() + 25;
  if (allResWidth > bodyWidth)
    $('.body-content').css('max-width', allResWidth);

  var usersWidth = $('#all-users').width() + 25;
  if (usersWidth > bodyWidth)
    $('.body-content').css('max-width', usersWidth);

  $('.svg-edit').click(function () {
    $(this).hide();
    $(this).siblings('.svg-cancel').show();
    $('.form-dic-editor').slideDown();
  });

  $('.svg-cancel').click(function () {
    $(this).hide();
    $(this).siblings('.svg-edit').show();
    $('.form-dic-editor').slideUp();
  });

  $(".toggle-skip").change(function () {
    var chb = $(this);
    var checked = this.checked;
    var cardId = chb.attr('data-id');
    var isDirect = chb.attr('data-is-direct');

    var body = "cardId=" + cardId + "&isDirect=" + isDirect + "&skip="+!this.checked;
    $.post('/user/SkipCard', body).done(function (data) {
      if (data === 'successful')
        var row = chb.parent().parent();
      var status = row.children().eq(3);

      if (checked) {
          row.removeClass('grey');
          status.text(status.attr('data-to-be-studied'));
        }
        else {
          row.addClass('grey');
          status.text(status.attr('data-skipped'));
        }
    });
  });

  var dicTry = GetParameterByName('try');
  if (dicTry != null) TryOut();
  
    
  $('#dd-lang div').on('click', (event) => {
    var language = $(event.currentTarget).text().trim();
    ChangeLanguage(language);
  }); 

  $(document).mouseup(function (e) {
    var container = $("#mob-menu"); 
    var home = $('.home');

    if (!container.is(e.target) && container.has(e.target).length === 0 
      && !home.is(e.target) && home.has(e.target).length === 0) {
      HideMenu();
    }

    var container2 = $("#dd-lang");
    if (!container2.is(e.target) && container2.has(e.target).length === 0) {
      $("#dd-lang").hide();
    }    
  });


  $('body').click(function (evt) {
    // mode changing dropdown list
    $('#mode-dd').html('').hide();
  });

  $('div[contenteditable="true"]').on('paste', function (e) {
    e.preventDefault();
    var pasteData = e.originalEvent.clipboardData.getData('text');
    document.execCommand("insertHTML", false, pasteData);
  });

  $('#errors').click(function () { $(this).empty() });

  $('#button-save-card').click(function () { SaveCard(); });
  $('#button-delete-card').click(function () { DeleteResult(); });

  $('#button-save-result').click(function () { SaveResult(); });
  $('#button-delete-result').click(function () { DeleteResult(); });

  $('#dic-delete').click(function () { DeleteDictionary(); });
  $('#mob-menu-icon').click(function () { HomeClick(); });
  $('#link-mode-button').click(function () { ShowModeSelect(); });
  $('.link-try-out').click(function () { TryOut(); });

  $('#one-many-toggle').change(function () { OneManyChanged(); });
  $('#change-mode #dicId').change(function () { ModeChanged(); });
  $('#change-mode #isDirect').change(function () { ModeChanged(); });

  $('#add-mixed-mode').click(function () { AddMixedMode(); });
  $('#save-mixed-mode').click(function () { SaveMixedMode(); });
  $('.mixed-delete').click(function () { RemoveMixedMode(this); });

  $('#select-lang').click(function () {
    $('#dd-lang').css('display', 'inline-block');
  });

  $('#mini-stat-outer').dblclick(function () {
    $('#mini-stat-container').toggle();
  });

  $('.random-switcher').click(function () {
    ChangeRandomState(this);
  });

  $('.limit-input').keypress(function (e) {
    if (isNaN(String.fromCharCode(e.which))) e.preventDefault();
  });

  $('#cards-content').keydown(function (e) {
    if (e.keyCode === 9) {
      var start = this.selectionStart;
      end = this.selectionEnd;

      var $this = $(this);
      $this.val($this.val().substring(0, start)
        + "\t"
        + $this.val().substring(end));

      this.selectionStart = this.selectionEnd = start + 1;
      return false;
    }
  });

  $('.logout').click(function () { Logout(); });

  DrawCharts();
};

function EnableTab(e) {
 
}

window.addEventListener("keydown", function (e) {
  if (e.keyCode == 27) {
    $('#mode-dd').html('').hide();
  }
}, true);

function EditDictionary(event) {
  event.preventDefault();
  var body = $('.dic-editor').serialize();

  $.post('/dictionaries/edit', body).done(function (data) {
    if (data === 'successful') {
      $('h1 span').first().text($('#Name').val());
      $('.svg-edit').show();
      $('.svg-cancel').hide();
      $('.form-dic-editor').slideUp();
    }
  });
}

function is_touch_device() {
  return 'ontouchstart' in window   // works on most browsers 
      || navigator.maxTouchPoints;  // works on IE10/11 and Surface
};





function HomeClick() {
  $('#shadow').toggle();
  $("#mob-menu").slideToggle(200);
}

function HideMenu() {
  $("#mob-menu").slideUp(200);
  $('#shadow').hide();
}



function TryOut() {
  $("html, body").animate({ scrollTop: 0 }, 100);
  tryingOut = true;
  $('#welcome').hide();
  $('#choose-training').show();
}




function StartTraining(number) {
  $('.info-message').hide();
  $('#studying ').css('display','flex');
  $('#choose-training').hide();
  $('#loading-img').show();
  $('#select-lang').hide();
  if (number == 1) GetCards();
  else GetChCards();
  if (tryingOut) setCookie('guestdic', $('#trying-dic').val(), 365)
  else UpdateMiniStat();
}

function GetChCards() {
  var body = null;
  var dicId = $('#trying-dic').val();
  if (dicId) body = 'dicId=' + dicId;

  $.post('/core/GetChooseCards', body).done(function (arr) {
    arr.forEach(function (c4) {
      chAll.push(c4);
    });
    if (!chStarted) {
      $('#loading-img').hide();
      $('#mode-2-content').show();
      chStarted = true;
      NewChCard();
    }
  });
}

function shuffle(a, b, c, d) {
  c = a.length; while (c) b = Math.random() * c-- | 0, d = a[c], a[c] = a[b], a[b] = d
}

var curTime;
function NewChCard() {
  if (chAll.length === 0) GetChCards();
  else {
    curCh = chAll.splice(0, 1)[0];
    curCh[0].origin = true;
    $('#ch-word').text(curCh[0].Word);
    shuffle(curCh);
    curCh.sort(function (a, b) { return getTextWidth(a.Translation) - getTextWidth(b.Translation); })

    for (var i = 0; i < curCh.length; i++)
      $('#ch-' + i).text(curCh[i].Translation);
    curTime = new Date();

    if (chAll.length <= 2) GetChCards();
  }
}

function getTextWidth(text) {
  var font = "bold 14pt arial";
  var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
  var context = canvas.getContext("2d");
  context.font = font;
  var metrics = context.measureText(text);
  return metrics.width;
}

function Choose(num) {
  if (waitingCorrect) return;
  else if (error) {
    error = false;
    $('#ch-error-translation').text('').hide();
    for (var i = 0; i <= 3; i++) {
      $('#ch-' + i).removeClass("ch-green").removeClass("ch-red");
    }
    NewChCard();
    return;
  }
  else if (curCh[num].origin == true) {
    waitingCorrect = true;
    $('#ch-' + num).addClass("ch-green");
    setTimeout(function () {
      $('#ch-' + num).removeClass("ch-green");
      NewChCard();
      waitingCorrect = false;
    }, 500);
    if (!tryingOut) SendChResult(num, true, new Date() - curTime);
  }
  else {
    error = true;
    $('#ch-error-translation').text(curCh[num].Translation + ' - ' + curCh[num].Word).show();

    $('#ch-' + num).addClass("ch-red");
    for (var i = 0; i < curCh.length; i++) {
      if (curCh[i].origin == true)
        $('#ch-' + i).addClass("ch-green");
    }
    if (!tryingOut) SendChResult(num, false, new Date() - curTime);
  }  
}

function SendChResult(num, correct, time) {
  cardsCnt++;
  var body = 'cardId=' + curCh[num].Id
    + '&isDirect=' + curCh[num].IsDirect
    + '&isCorrect=' + correct
    + '&time=' + time;
  $.post('/core/sendChooseResult', body).done(function (data) {
    UpdateMiniStat(data);
    $('#errors').html('');
  }).fail(function () {
    setTimeout(function () {
      SendChResult(num, correct, time);
    }, 3000);
    $('#errors').html('Error sending result to the server ' + curCh[num].Id + ' ' + curCh[num].Word + ' (' + new Date() + ')');
  });
}






function NewCard() {
  if (cards.length === 0) GetCards();
  else {
    card = cards.splice(0, 1)[0];
    card.VWord = card.IsDirect ? card.Word : card.Translation;
    card.VTranslation = card.IsDirect ? card.Translation : card.Word;
    card.VTranscription = (card.IsDirect && card.Transcription) ? ' ' + card.Transcription : '';
    card.hints = 0;

    if (card.Example) {
      $('#button-show-example').show();
      $('#example').html(card.Example.replace(/\*(.+?)\*/g, '<span class="highlighted">$1</span>')).hide();
    }
    else {
      $('#button-show-example').hide();
      $('#example').html('').hide();
    }

    $('#word').html(card.VWord.replace(/\|/g, '<br>'));
    $('#transcription').html(card.VTranscription);
    $('#translation').html(" ");
    $('#card-info').html(card.CardInfo);

    $('#button3').prop('disabled', false);
    $('#button2').prop('disabled', false);
    if (cards.length <= 2) GetCards();
  }
}

function GetCards() {
  var body = null;
  var dicId = $('#trying-dic').val();
  if (dicId) body = 'dicId=' + dicId;

  $.post('/core/getCards', body)
    .done(function (arr) {
      var Cards = arr.Cards
      $('#ms-get-cards').text(arr.Info);
      
      for (var i = 0; i < Cards.length; i++) {
        var exists = false;
        cards.forEach(function (w) {
          if (w.CardId == Cards[i].CardId || (card !== undefined && Cards[i].CardId === card.CardId))
            exists = true;
        });
        if (!exists) cards.push(Cards[i]);
      }
      
      if (!started) {
        $('#loading-img').hide();
        $('#mode-1-content').show();
        started = true;
        NewCard();
        $('#buttons-row-show-hint').show();
      }
    }).fail(function () {
      console.log('request failed');
    });
}

function SendResult(type) {
  cardsCnt++;
  $('#buttons-row-show-hint').show();
  $('#buttons-row-assess').hide();
  $('#buttons-row-bottom').hide();
  $('#card-container').show();
  $('#edit-container').hide();
  $('#button-save-card').text($('#button-save-card').attr('data-val1'));
  $('#button-delete-card').val($('#button-delete-card').attr('data-val1'));

  SendTheResult(type, card.CardId, card.IsDirect);


  NewCard();
  $('#button-edit').prop('disabled', false);
  $('#button-edit').text($('#button-edit').attr('data-val1'));
  $('#button-save-card').prop('disabled', false);
  $('#button-delete-card').prop('disabled', false);
  $('#button-mark').prop('disabled', false);
  $('#button-mark').text($('#button-mark').attr('data-val1'));
}

function SendTheResult(type, cardId, isDirect) {
  var word2 = card.word;
  var body = 'cardId=' + cardId + '&isDirect=' + isDirect + '&result=' + type;
  $.post('/core/sendResult', body).done(function (data) {
    UpdateMiniStat(data);
    $('#example').html('');
  }).fail(function () {
    setTimeout(function () {
      SendTheResult(type, cardId, isDirect);
    }, 3000);
    $('#errors').html('Error sending result to the server ' + card.CardId + ' ' + word2 + ' (' + new Date() + ')');
  });
}

function UpdateMiniStat(stat) {
  if (stat !== undefined) SetMiniStat(stat);
  else {
    $.post('/core/getDicStat').done(function (stat) {
      SetMiniStat(stat);
    });
  }
}

function SetMiniStat(stat) {
  if (stat.DicChooseAvailable === true) $('#variant-choose').show();
  else $('#variant-choose').hide();

  $('#mini-stat-updating').hide();
  $('#mini-stat').show();

  if (initStat == undefined) initStat = stat;

  var _dir = stat.IsDirect == true ? '' : " (" + $('#mode-name').attr('data-indir') + ")";
  $('#mode-name').text(stat.DicName + _dir);
  if (stat.IsInMix) $('.mixed').show();
  else $('.mixed').hide();

  $("#hs-old").text(stat.Old);
  var el = $("#ms-old").text(stat.Old).removeClass();
  if (stat.Old < prevStat.Old) el.addClass('green');
  if (stat.Old > prevStat.Old) el.addClass('red');

  $("#hs-weak-ready").text(stat.WeakReady);
  var el = $("#ms-weak-ready").text(stat.WeakReady).removeClass();
  if (stat.WeakReady < prevStat.WeakReady) el.addClass('green');
  if (stat.WeakReady > prevStat.WeakReady) el.addClass('red');

  var el = $("#ms-weak").text(stat.Weak).removeClass();
  if (stat.Weak < prevStat.Weak) el.addClass('green');
  if (stat.Weak > prevStat.Weak) el.addClass('red');

  var el = $("#ms-mid").text(stat.Mid).removeClass();
  if (stat.Mid < prevStat.Mid && stat.Weak <= prevStat.Weak) el.addClass('green');
  if (stat.Mid > prevStat.Mid && stat.Weak >= prevStat.Weak) el.addClass('red');

  $("#hs-count").text(stat.Studied);
  var el = $("#ms-count").text(stat.Studied).removeClass();
  if (stat.Studied < prevStat.Studied) el.addClass('red');
  if (stat.Studied > prevStat.Studied) el.addClass('green');

  $("#hs-total").text(stat.Studying);
  var el = $("#ms-total").text(stat.Studying).removeClass();
  if (stat.Studying < prevStat.Studying) el.addClass('red');
  if (stat.Studying > prevStat.Studying) el.addClass('green');

  var AvgStr = $("#ms-strength").html('0.');

  var avs = parseFloat(stat.AvgStrength).toFixed(4);
  var pavs = parseFloat(prevStat.AvgStrength).toFixed(4);
  var color = '';

  var check = true;
  for (var i = 2; i < avs.length; i++) {
    var avsi = parseInt(avs[i]);

    var el = $("<span>").text(avsi);

    if (!check) {
      el.addClass(color);
    }
    else if (avsi < parseInt(pavs[i])) {
      check = false;
      color = 'red';
      el.addClass(color);
    }
    else if (avsi > parseInt(pavs[i])) {
      check = false;
      color = 'green';
      el.addClass(color);
    }
    AvgStr.append(el);
  }

  var lws = $('#ms-last-card');
  if (stat.LastResult && lws[0]) {
    var newStrength = parseFloat(stat.LastResult.NewStrength.toFixed(2));
    var oldStrength = parseFloat(stat.LastResult.OldStrength.toFixed(2));
    var strengthDiff = parseFloat((stat.LastResult.NewStrength - stat.LastResult.OldStrength).toFixed(2));
    var oldId = stat.LastResult.Id;
    lws.html('    ');
    lws.append(oldStrength.toFixed(2)).append(' ');

    var el2 = $("<span>");
    el2.append(strengthDiff.toFixed(2)).append(' = ');
    el2.append('<a target="_blank" href="/results/edit/' + oldId + '">' + newStrength.toFixed(2) + '</a>');

    if (strengthDiff > 0) {
      el2.prepend('+');
      el2.addClass('green');
    }
    else if (strengthDiff < 0) el2.addClass('red');

    lws.append(el2)
  }
  prevStat = stat;
  SetStatDifference(stat);
  ShrinkHeader();
}

function SetStatDifference(stat) {
  if (initStat == null) return;

  $("#msd-cards-cnt").text(cardsCnt);

  var diff = stat.Old - initStat.Old;
  diff = AddSpaces(diff, initStat.Old);
  var el = $("#msd-old").text(diff).removeClass();
  if (diff < 0) el.addClass('green');
  if (diff > 0) el.addClass('red');

  var diff = stat.WeakReady - initStat.WeakReady;
  diff = AddSpaces(diff, initStat.WeakReady);
  var el = $("#msd-weak-ready").text(diff).removeClass();
  if (diff < 0) el.addClass('green');
  if (diff > 0) el.addClass('red');

  var diff = stat.Weak - initStat.Weak;
  diff = AddSpaces(diff, initStat.Weak);
  var el = $("#msd-weak").text(diff).removeClass();
  if (diff < 0) el.addClass('green');
  if (diff > 0) el.addClass('red');

  var diff = stat.Mid - initStat.Mid;
  diff = AddSpaces(diff, initStat.Mid);
  var el = $("#msd-mid").text(diff).removeClass();
  if (diff < 0) el.addClass('green');
  if (diff > 0) el.addClass('red');////
  
  var diff = stat.Studied - initStat.Studied;
  diff = AddSpaces(diff, initStat.Studied);
  var el = $("#msd-count").text(diff).removeClass();
  if (diff < 0) el.addClass('red');
  if (diff > 0) el.addClass('green');

  var diff = stat.Studying - initStat.Studying;
  var diffAvg = stat.AvgStrength - initStat.AvgStrength;
  var correction = diffAvg === 0 ? 0 : -1;
  diff = AddSpaces(diff, initStat.Studying, correction);
  var el = $("#msd-total").text(diff).removeClass();
  if (diff < 0) el.addClass('red');
  if (diff > 0) el.addClass('green');

  var diff = stat.AvgStrength - initStat.AvgStrength;
  var el = $("#msd-strength").text(diff.toFixed(4)).removeClass();
  if (diff < 0) el.addClass('red');
  if (diff > 0) el.addClass('green').prepend('+');

}

function AddSpaces(diff, value, correction) {
  var d = Math.floor(Math.log10(Math.abs(diff)));
  if (diff == 0) d = 0;
  var spaces = Math.floor(Math.log10(value)) - d;

  if (correction > 0 || correction < 0) spaces += correction;

  if (diff > 0) {
    diff = '+' + diff;
    spaces--;
  }
  if (diff < 0) {
    if (spaces > 0) diff = ' ' + diff;
    spaces--;
  }

  if (spaces == 1) return ' ' + diff + '';
  if (spaces == 2) return ' ' + diff + ' ';
  if (spaces == 3) return '  ' + diff + ' ';
  if (spaces == 4) return '  ' + diff + '  ';
  return diff;
}

function ButHint() {
  card.hints++;
  if (card.hints > 1) $('#button3').prop('disabled', true);
  if (card.hints > 3) $('#button2').prop('disabled', true);
  var translMasked = '';
  for (var i = 0; i < card.VTranslation.length; i++) {
    if (card.hints === card.VTranslation.length) {
      ButShowTranslation();
      return;
    }
    if (card.hints <= i && card.VTranslation[i] !== ' ') translMasked += '_';
    else translMasked += card.VTranslation[i];
  }
  $('#translation').html(translMasked);
}

function ButShowTranslation() {
  $('#buttons-row-show-hint').hide();
  $('#buttons-row-assess').show();
  $('#buttons-row-bottom').show();
  $('#translation').html(card.VTranslation.replace(/\|/g, '<br>'));
}


function ButShowExample() {
  if ($('#example').css('display') == 'none') {
    $('#example').show();
    $('#button-show-example').text($('#button-show-example').attr('data-val2'));
  }
  else {
    $('#example').hide();
    $('#button-show-example').text($('#button-show-example').attr('data-val1'));
  }
}

function ButMark() {
  var body = 'CardId=' + card.CardId;
  $.post('/cards/mark', body).done(function (data) {
    if (data === 'successful') $('#button-mark').text($('#button-mark').attr('data-val2'));
  })
  $('#button-mark').prop('disabled', true);
}

function ButEdit() {
  if ($('#mode-1-content').is(':visible')) {
    $('#mode-1-content').hide();
    $('#edit-container').show();
    $('#button-save-card').text($('#button-save-card').attr('data-val1'));
    $('#button-delete-card').val($('#button-delete-card').attr('data-val1'));
    $('#button-edit').text($('#button-edit').attr('data-val2'));

    $('#Id').val(card.CardId);
    $('#Word').text(card.Word);
    $('#Transcription').text(card.Transcription);
    $('#Translation').text(card.Translation);
    $('#Example').val(card.Example);
  }
  else {
    $('#mode-1-content').show();
    $('#edit-container').hide();
    $('#button-edit').text($('#button-edit').attr('data-val1'));
  }
}

function SaveCard() {
  $('#button-save-card').text($('#button-save-card').attr('data-val1'));

  card.Word = $('#Word').text();
  card.Translation = $('#Translation').text();
  card.Transcription = $('#Transcription').text();
  card.Example = $('#Example').val();

  card.VWord = card.IsDirect ? card.Word : card.Translation;
  card.VTranslation = card.IsDirect ? card.Translation : card.Word;
  card.VTranscription = (card.IsDirect && card.Transcription) ? ' ' + card.Transcription : '';

  $('#word').html(card.VWord.replace(/\|/g, '<br>'));
  $('#transcription').html(card.VTranscription);
  $('#translation').html(card.VTranslation);
  $('#example').html(card.Example.replace(/\*(.+?)\*/g, '<span class="highlighted">$1</span>')).hide();

  var body = $("#form-edit-result").serialize()
    + "&Word=" + card.Word
    + "&Translation=" + card.Translation
    + "&Transcription=" + card.Transcription;
  $.post('/cards/edit', body).done(function (data) {
    if (data === 'successful')
      $('#button-save-card').text($('#button-save-card').attr('data-val2'));
  });
}


function DeleteCard() {
  if (!confirm("Delete card?")) return false;

  $('#button-delete-card').val($('#button-delete-card').attr('data-val1'));
  var Id = $("#form-edit-result input[type='hidden']").val();
  var body = 'Id=' + Id;
  $.post('/cards/delete', body).done(function (data) {
    if (data === 'successful') $('#button-delete-card').val($('#button-delete-card').attr('data-val2'));
    $('#button-delete-card').prop('disabled', true);
    $('#button-save-card').prop('disabled', true);
  });
}

function SaveResult() {
  var body = $("#form-edit-result").serialize();
  $.post('/results/edit', body).done(function (data) {
    if (data === 'successful')
      $('#button-save-result').text($('#button-save-result').attr('data-val2'));
  });
}

function DeleteResult() {
  if (!confirm("Delete result?")) return false;

  var body = 'Id=' + $('#Id').val();
  $.post('/results/delete', body).done(function (data) {
    if (data === 'successful') {
      $('#button-delete-result').text($('#button-delete-result').attr('data-val2'));
    }
    $('#button-delete-result').prop('disabled', true);
    $('#button-save-result').prop('disabled', true);
  });
}

function DeleteDictionary() {
  if (confirm($('#dic-delete').attr('data-confirmation'))) {
    var dicId = $('#dic-delete').attr('data-dic-id');
    var body = 'id=' + dicId;
    $.post('/dictionaries/delete', body).done(function (data) {
      if (data === 'successful') window.location.replace('/dictionaries');
    });
  }
  return false;
}

function ShowModeSelect() {
  $('#loading-small').show();
  $.post('/core/GetModes').done(function (modes) {
    $('#loading-small').hide();
    $('#mode-dd').html('');
    
    for (var i = 0; i < modes.length; i++)
    {
      var line = $('<div></div>').text(modes[i].DicName).attr('data-dic-id', modes[i].DicId).attr('data-is-direct', modes[i].IsDirect);
      if (modes[i].IsCurrent) line.addClass('bold');
      $('#mode-dd').append(line);
    }

    $('#mode-dd').show();
    $('.shading').show();

    $('#mode-dd div').on('click', function (event) {
      var ct = $(event.currentTarget);
      PanelChangeDic(ct.attr('data-dic-id'), ct.attr('data-is-direct'));
    });
  });
}


function PanelChangeDic(dicId, isDirect) {
  if (dicId == -999) {
    $.post('/user/SwitchToMixedMode').done(function (data) {
      if (data === 'successful') location.reload();
    });
    return;
  }

  var body = 'dicId=' + dicId + '&isDirect=' + isDirect + '&needRedirect=true';
  $.post('/user/changeMode', body).done(function (data) {
    if (data === 'successful' && RefreshOnModeChanged === true) location.reload();
    else UpdateMiniStat();
  });
}

function StudyDictionary(dicId) {
  var body = 'dicId=' + dicId + '&isDirect=true&needRedirect=true';
  $.post('/user/changeMode', body).done(function (data) {
    if (data === 'successful') window.location.replace('/');
  });
}




function ModeChanged() {
  $('#limit').val('');
  $('#randomOrder').prop('checked', false);
  var body = $("#change-mode").serialize();
  $.post('/user/GetModeSettings', body).done(function (data) {
    if (data.StudyLimit == 0) data.StudyLimit = '';
    $('#limit').val(data.StudyLimit);
    $('#randomOrder').prop('checked', data.RandomOrder);
  });

}


function Logout() {
  $.post('/user/logout').done(function () {
    location.reload();
  });
}


function setCookie(name, value, days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}
function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}
function eraseCookie(name) {
  document.cookie = name + '=; Max-Age=-99999999;path=/';
}

function GetParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}


function DrawCharts() {
  if ($('#chart-ranks').length != 1) return;

  google.charts.load('current', { packages: ['corechart', 'line'] });
  google.charts.setOnLoadCallback(drawChart1);

  google.charts.load('current', { 'packages': ['corechart'] });
  google.charts.setOnLoadCallback(drawChart2);

  function drawChart1() {
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'X');
    data.addColumn('number', $('#chart-ranks').attr('data-count'));

    data.addRows(ranks_data);

    var options = {
      height: 260,
      width: $('#chart-ranks').width(),
      hAxis: {
        title: $('#chart-ranks').attr('data-strength'),
        gridlines: { count: 10 },
        titleTextStyle: { italic: false }
      },
      vAxis: {
        title: $('#chart-ranks').attr('data-count'),
        titleTextStyle: { italic: false }
      },
      colors: ['#325ecf'],
      legend: { position: 'none' },
      fontName: 'arial',
      chartArea: { width: '90%', height: '80%', top: 8, left: 42 }
    };
    var chart = new google.visualization.LineChart(document.getElementById('chart-ranks'));
    chart.draw(data, options);
  }

  /*days*/
  function drawChart2() {
    var data = new google.visualization.DataTable();
    data.addColumn('date', $('#chart-days').attr('data-days'));
    data.addColumn('number', $('#chart-days').attr('data-count'));

    data.addRows(days_data);

    var options2 = {
      height: 260,
      width: $('#chart-days').width(),
      hAxis: {
        title: $('#chart-days').attr('data-days'),
        format: 'MMM y',
        gridlines: { count: -1 },
        titleTextStyle: { italic: false }
      },
      vAxis: {
        title: $('#chart-days').attr('data-count'),
        titleTextStyle: { italic: false }
      },
      colors: ['#325ecf'],
      legend: { position: 'none' },
      fontName: 'arial',
      chartArea: { width: '90%', height: '80%', top: 8, left: 42 }
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('chart-days'));
    chart.draw(data, options2);
  }
}

function ChangeLanguage(language) {
  $.post('/user/ChangeLanguage', 'language=' + language).done(function (data) {
    if (data === 'successful') location.reload();
  })
}

function SearchCards(event) {
  if (event != 'search' && event.keyCode !== 13) return;

  $('.search-cards').focus();
  var dicId = $('.search-cards').first().attr('data-dic-id');
  var text = $('.search-cards').first().val();
  $('#table-dic-words a').contents().unwrap();

  var body = "dicId=" + dicId + "&text=" + text;
  $.post('/cards/search', body).done(function (data) {
    $('#table-dic-words tbody tr:not(:first)').remove();
    $('.page-group').hide();

    for (var i = 0; i < data.length; i++) {
      var e = data[i];
      var transcr = '';
      if (e.Transcription) transcr = ' ' + e.Transcription;
      if (e.Transcription) transcr = ' ' + e.Transcription;
      $('#table-dic-words tbody').append('<tr><td><a href="/cards/edit/'+e.Id+'">'+(i + 1)+'</a></td><td>' + e.Word + transcr + '</td><td>' + e.Translation + '</td></tr>')
    }    
  });
}

function OneManyChanged() {
  $('#change-mode').toggle();
  $('#mixed-container').toggle();
  $('#one-many-toggle label').toggleClass('bold');
}

function AddMixedMode() {
  var modeId = $('#mixed-id').val();
  var modeName = $('#mixed-id option:selected').text();
  var isDirect = $("input[name='mixed-direction']:checked").val();
  var dirName = $("input[name='mixed-direction']:checked").attr('data-dir-name');

  var arr = $('#mixed-list > div').map(function () {
    return [[$(this).attr('data-id'), $(this).attr('data-direct')]];
  }).get();

  var duplication = false;
  arr.forEach((mode) => {
    if (+mode[0] === +modeId && +mode[1] === +isDirect) {
      duplication = true;
    }
  });
  if (duplication) {
    $('.mode-exists').show();
    return;
  }

  var div = $('<div>').attr('data-id', modeId).attr('data-direct', isDirect);

  var link = $('<a>').attr('href', '/dictionaries/view/' + modeId).attr('target', '_blank').text(modeName);  
  div.append($('<span>').append(link));
  div.append($('<span>').text(dirName).addClass('non-selectable'));

  var studyLimit = $('<span>').addClass('limit-input').attr('contenteditable', 'true');
  var randomOrder = $('<span>').addClass('random-switcher').click(function () { ChangeRandomState(this); });
  div.append(studyLimit).append(randomOrder);
  
  var x = $('<span>').text(' × ').addClass('mixed-delete')
    .click(function(e) { RemoveMixedMode(e.currentTarget) });
  div.append(x);  

  $('#mixed-list-empty').hide();
  $('#mixed-list').append(div);
  $('.mode-exists').hide();

  var body = "dicId=" + modeId + "&isDirect=" + isDirect;
  $.post('/user/GetModeSettings', body).done(function (data) {
    studyLimit.text(data.StudyLimit);
    randomOrder.text(data.RandomOrder ?
      $('#mixed-list').attr('data-random') : $('#mixed-list').attr('data-normal'));
    randomOrder.attr('data-val', data.RandomOrder);
  });
}

function RemoveMixedMode(element) {
  $(element).parent().remove();

  if ($('#mixed-list > div').length === 0)
    $('#mixed-list-empty').show();
}

function ChangeRandomState(el) {
  var random = $(el).attr('data-val').toString().toLowerCase() == 'true';
  $(el).attr('data-val', !random);
  $(el).text($('#mixed-list').attr(random ? 'data-normal' : 'data-random'));
}

function SaveMixedMode() {
  var arr = $('#mixed-list > div').map(function () {
    var obj = {};    
    obj.DicId = $(this).attr('data-id');
    obj.IsDirect = $(this).attr('data-direct');
    obj.StudyLimit = $(this).find('.limit-input').text();
    obj.RandomOrder = $(this).find('.random-switcher').attr('data-val');
    return obj;
  }).get();

  $.ajax({
    url: '/user/ChangeMixedMode',
    type: "POST",
    data: JSON.stringify(arr),
    dataType: "json",
    contentType: 'application/json; charset=utf-8',
    complete: function (data) {
      if (data.responseText === 'successful') location.reload();
    }
  });
}










