﻿using Apprendre.Models;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Apprendre.Utils
{
  public class NinjectRegistrations : NinjectModule
  {
    public override void Load()
    {
      Unbind<ModelValidatorProvider>();

      Bind<IDictionaryService>().To<DictionaryService>();
      Bind<ICoreService>().To<CoreService>();
      Bind<IResultService>().To<ResultService>();
      Bind<ICardService>().To<CardService>();
      Bind<IUserService>().To<UserService>();
      Bind<IStatService>().To<StatService>();
    }
  }
}