##Improve Your Vocabulary

http://improveyourvocabulary.somee.com/

---

This project is similar to [Anki](https://apps.ankiweb.net/), it helps to learn foreign words using [Spaced repetition](https://en.wikipedia.org/wiki/Spaced_repetition)

Used ASP.NET MVC 5.2.6 & Entity Framework 6.0

---

###Screenshots

![main_page](https://a.radikal.ru/a17/1901/6f/ac1b0bcb3c57.png)

---

![training_1](https://a.radikal.ru/a19/1901/ce/89e0ec3b3a2a.png)

---

![user's_profile_page](https://c.radikal.ru/c20/1901/dd/30a8b677c5c3.png)

---

![users's_statistics_page](https://a.radikal.ru/a13/1901/34/9d65fe4b16e3.png) 

---

![dictionary_page](https://d.radikal.ru/d02/1901/b4/16ca87a3095d.png)

---

![card_edition_page](https://d.radikal.ru/d35/1901/07/51c3c0a9dea6.png)


